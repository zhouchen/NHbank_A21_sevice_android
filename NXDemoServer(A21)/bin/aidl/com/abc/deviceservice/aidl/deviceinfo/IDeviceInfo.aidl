

package com.abc.deviceservice.aidl.deviceinfo;



interface IDeviceInfo {

int connectDevice();
void disConnectDevice();
String getSN();	
String getVID();	
String getVName();	
String getFirmwareVersion(); 	 
String getDeviceModel(); 	
boolean getConnectState();
void cancelOperation();

}