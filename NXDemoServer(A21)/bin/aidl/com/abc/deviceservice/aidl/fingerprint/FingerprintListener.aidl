
package com.abc.deviceservice.aidl.fingerprint;

interface FingerprintListener {
/**
     * 结果返回
*@param data:结果数据 - {“data”:”指纹数据”,”vendorCode”:”厂商编号”}
        JSON格式toString
*       data:指纹特征值数据
维尔：WELLCOM
中正：MIAXIS
天诚盛业：TECHSHINO

     */


    void onResult(String data);
/**
* 失败
*
* @param Errmessage - 错误描述 -JSON格式toString
   code:错误码
   message:错误信息
*/
    void onError(String Errmessage);

}