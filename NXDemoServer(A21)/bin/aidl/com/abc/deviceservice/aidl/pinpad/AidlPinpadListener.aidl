package com.abc.deviceservice.aidl.pinpad;

interface AidlPinpadListener{
  
  
    
   //data JSON格式字符串
    void onResult(String data);
    
   
    void onError(String errorDescription);
    
    
}
