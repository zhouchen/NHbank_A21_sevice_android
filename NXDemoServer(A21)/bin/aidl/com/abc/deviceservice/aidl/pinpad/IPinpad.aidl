package com.abc.deviceservice.aidl.pinpad;
import com.abc.deviceservice.aidl.pinpad.AidlPinpadListener;
interface IPinpad{

boolean loadMainKey(String publicKeyName,String content,int code,int cipherType,int timeOut);

boolean loadWorkKey(String content,int mainKeyCode,int code,int cipherType ,int timeOut);	

void getPin (int inputTimes,String bankCardNo,int cipherType,int timeOut, AidlPinpadListener listener);	
                                                                          
String getPublicKey(String name);

}