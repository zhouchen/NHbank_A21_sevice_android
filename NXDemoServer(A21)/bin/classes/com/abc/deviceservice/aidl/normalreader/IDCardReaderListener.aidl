package com.abc.deviceservice.aidl.normalreader;

import android.os.Bundle;

 interface IDCardReaderListener {
/**
	* 读取身份证成功
	* 
	* @param errmessage -JSON格式toString
	  data:标识符
	*/

	void onIDCardReaderResult(String data);
	  
	/**
	* 读取身份证失败
	* 
	* @param errmessage -JSON格式toString
	  code错误码
	  message:为错误信息
	*/
	void onError(String errmessage);
	}
