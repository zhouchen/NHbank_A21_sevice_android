package com.abc.deviceservice.aidl.normalreader;
import com.abc.deviceservice.aidl.normalreader.NormalCardReaderListener;
import com.abc.deviceservice.aidl.normalreader.ICCardARQCListener;
import com.abc.deviceservice.aidl.normalreader.WriteICCardListener;
import com.abc.deviceservice.aidl.normalreader.IDCardReaderListener;

interface INormalCard{

void  normalCardReader(int readMode, NormalCardReaderListener listener,int timeout);	
void  getICCardARQC(int readMode,String options, ICCardARQCListener listener,int timeout);//3.4.3.2.	读IC55域信息	
void  writeICCard(String content,String script, WriteICCardListener Listener,int timeout);	
void  idCardReader(int readMode,IDCardReaderListener listener,int timeout);	//身份证  readMode 0-身份证  其他-外国人居留

}
