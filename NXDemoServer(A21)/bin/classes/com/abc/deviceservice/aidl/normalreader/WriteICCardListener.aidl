package com.abc.deviceservice.aidl.normalreader;

import android.os.Bundle;

 interface WriteICCardListener {
    /**
     * 结果返回
     *@param bundle:写卡结果数据
     *ScriptResult（Sting） 校验结果
     *TC （Sting）交易证书
     */
void onNormalCardReaderResult(in Bundle bundle);
/**
* 读卡失败
* @param error - 错误码
* @param message - 错误描述
*/
void onError(int error, String message);

}
