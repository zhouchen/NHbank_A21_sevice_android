package com.abc.deviceservice.aidl.signature;

 interface SigntrueListener{
    /**
     * 结果返回
     *@param data:结果数据 JSON格式toString
      data为数据信息
     */
void onResult(String data);
/**
* 失败
* 
* @param message - JSON格式toString
code为错误码
message错误信息
*/
void onError( String message);

}

