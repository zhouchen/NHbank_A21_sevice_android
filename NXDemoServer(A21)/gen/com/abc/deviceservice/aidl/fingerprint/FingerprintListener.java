/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: E:\\eclipseworkspace\\NXDemoServer(A21)\\src\\com\\abc\\deviceservice\\aidl\\fingerprint\\FingerprintListener.aidl
 */
package com.abc.deviceservice.aidl.fingerprint;
public interface FingerprintListener extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.abc.deviceservice.aidl.fingerprint.FingerprintListener
{
private static final java.lang.String DESCRIPTOR = "com.abc.deviceservice.aidl.fingerprint.FingerprintListener";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.abc.deviceservice.aidl.fingerprint.FingerprintListener interface,
 * generating a proxy if needed.
 */
public static com.abc.deviceservice.aidl.fingerprint.FingerprintListener asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.abc.deviceservice.aidl.fingerprint.FingerprintListener))) {
return ((com.abc.deviceservice.aidl.fingerprint.FingerprintListener)iin);
}
return new com.abc.deviceservice.aidl.fingerprint.FingerprintListener.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_onResult:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
this.onResult(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_onError:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
this.onError(_arg0);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.abc.deviceservice.aidl.fingerprint.FingerprintListener
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
/**
     * 结果返回
*@param data:结果数据 - {“data”:”指纹数据”,”vendorCode”:”厂商编号”}
        JSON格式toString
*       data:指纹特征值数据
维尔：WELLCOM
中正：MIAXIS
天诚盛业：TECHSHINO

     */
@Override public void onResult(java.lang.String data) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(data);
mRemote.transact(Stub.TRANSACTION_onResult, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
* 失败
*
* @param Errmessage - 错误描述 -JSON格式toString
   code:错误码
   message:错误信息
*/
@Override public void onError(java.lang.String Errmessage) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(Errmessage);
mRemote.transact(Stub.TRANSACTION_onError, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_onResult = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_onError = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
}
/**
     * 结果返回
*@param data:结果数据 - {“data”:”指纹数据”,”vendorCode”:”厂商编号”}
        JSON格式toString
*       data:指纹特征值数据
维尔：WELLCOM
中正：MIAXIS
天诚盛业：TECHSHINO

     */
public void onResult(java.lang.String data) throws android.os.RemoteException;
/**
* 失败
*
* @param Errmessage - 错误描述 -JSON格式toString
   code:错误码
   message:错误信息
*/
public void onError(java.lang.String Errmessage) throws android.os.RemoteException;
}
