/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: E:\\eclipseworkspace\\NXDemoServer(A21)\\src\\com\\abc\\deviceservice\\aidl\\fingerprint\\IFingerprint.aidl
 */
package com.abc.deviceservice.aidl.fingerprint;
public interface IFingerprint extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.abc.deviceservice.aidl.fingerprint.IFingerprint
{
private static final java.lang.String DESCRIPTOR = "com.abc.deviceservice.aidl.fingerprint.IFingerprint";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.abc.deviceservice.aidl.fingerprint.IFingerprint interface,
 * generating a proxy if needed.
 */
public static com.abc.deviceservice.aidl.fingerprint.IFingerprint asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.abc.deviceservice.aidl.fingerprint.IFingerprint))) {
return ((com.abc.deviceservice.aidl.fingerprint.IFingerprint)iin);
}
return new com.abc.deviceservice.aidl.fingerprint.IFingerprint.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_readFinger:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
com.abc.deviceservice.aidl.fingerprint.FingerprintListener _arg1;
_arg1 = com.abc.deviceservice.aidl.fingerprint.FingerprintListener.Stub.asInterface(data.readStrongBinder());
this.readFinger(_arg0, _arg1);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.abc.deviceservice.aidl.fingerprint.IFingerprint
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
@Override public void readFinger(int timeout, com.abc.deviceservice.aidl.fingerprint.FingerprintListener listener) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(timeout);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_readFinger, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_readFinger = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
}
public void readFinger(int timeout, com.abc.deviceservice.aidl.fingerprint.FingerprintListener listener) throws android.os.RemoteException;
}
