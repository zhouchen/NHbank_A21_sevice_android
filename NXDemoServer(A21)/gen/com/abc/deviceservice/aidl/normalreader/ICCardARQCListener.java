/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: E:\\eclipseworkspace\\NXDemoServer(A21)\\src\\com\\abc\\deviceservice\\aidl\\normalreader\\ICCardARQCListener.aidl
 */
package com.abc.deviceservice.aidl.normalreader;
public interface ICCardARQCListener extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.abc.deviceservice.aidl.normalreader.ICCardARQCListener
{
private static final java.lang.String DESCRIPTOR = "com.abc.deviceservice.aidl.normalreader.ICCardARQCListener";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.abc.deviceservice.aidl.normalreader.ICCardARQCListener interface,
 * generating a proxy if needed.
 */
public static com.abc.deviceservice.aidl.normalreader.ICCardARQCListener asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.abc.deviceservice.aidl.normalreader.ICCardARQCListener))) {
return ((com.abc.deviceservice.aidl.normalreader.ICCardARQCListener)iin);
}
return new com.abc.deviceservice.aidl.normalreader.ICCardARQCListener.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_onNormalCardReaderResult:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
this.onNormalCardReaderResult(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_onError:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
this.onError(_arg0);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.abc.deviceservice.aidl.normalreader.ICCardARQCListener
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
/**
     * 结果返回
     *@param data:55域信息（TLV格式） jsonobject  string 格式
     */
@Override public void onNormalCardReaderResult(java.lang.String data) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(data);
mRemote.transact(Stub.TRANSACTION_onNormalCardReaderResult, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
* 读卡失败
* @param error - 错误码
* @param message - 错误描述
*/
@Override public void onError(java.lang.String str) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(str);
mRemote.transact(Stub.TRANSACTION_onError, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_onNormalCardReaderResult = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_onError = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
}
/**
     * 结果返回
     *@param data:55域信息（TLV格式） jsonobject  string 格式
     */
public void onNormalCardReaderResult(java.lang.String data) throws android.os.RemoteException;
/**
* 读卡失败
* @param error - 错误码
* @param message - 错误描述
*/
public void onError(java.lang.String str) throws android.os.RemoteException;
}
