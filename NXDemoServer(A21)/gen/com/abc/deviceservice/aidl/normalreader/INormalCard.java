/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: E:\\eclipseworkspace\\NXDemoServer(A21)\\src\\com\\abc\\deviceservice\\aidl\\normalreader\\INormalCard.aidl
 */
package com.abc.deviceservice.aidl.normalreader;
public interface INormalCard extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.abc.deviceservice.aidl.normalreader.INormalCard
{
private static final java.lang.String DESCRIPTOR = "com.abc.deviceservice.aidl.normalreader.INormalCard";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.abc.deviceservice.aidl.normalreader.INormalCard interface,
 * generating a proxy if needed.
 */
public static com.abc.deviceservice.aidl.normalreader.INormalCard asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.abc.deviceservice.aidl.normalreader.INormalCard))) {
return ((com.abc.deviceservice.aidl.normalreader.INormalCard)iin);
}
return new com.abc.deviceservice.aidl.normalreader.INormalCard.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_normalCardReader:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
com.abc.deviceservice.aidl.normalreader.NormalCardReaderListener _arg1;
_arg1 = com.abc.deviceservice.aidl.normalreader.NormalCardReaderListener.Stub.asInterface(data.readStrongBinder());
int _arg2;
_arg2 = data.readInt();
this.normalCardReader(_arg0, _arg1, _arg2);
reply.writeNoException();
return true;
}
case TRANSACTION_getICCardARQC:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
java.lang.String _arg1;
_arg1 = data.readString();
com.abc.deviceservice.aidl.normalreader.ICCardARQCListener _arg2;
_arg2 = com.abc.deviceservice.aidl.normalreader.ICCardARQCListener.Stub.asInterface(data.readStrongBinder());
int _arg3;
_arg3 = data.readInt();
this.getICCardARQC(_arg0, _arg1, _arg2, _arg3);
reply.writeNoException();
return true;
}
case TRANSACTION_writeICCard:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
java.lang.String _arg1;
_arg1 = data.readString();
com.abc.deviceservice.aidl.normalreader.WriteICCardListener _arg2;
_arg2 = com.abc.deviceservice.aidl.normalreader.WriteICCardListener.Stub.asInterface(data.readStrongBinder());
int _arg3;
_arg3 = data.readInt();
this.writeICCard(_arg0, _arg1, _arg2, _arg3);
reply.writeNoException();
return true;
}
case TRANSACTION_idCardReader:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
com.abc.deviceservice.aidl.normalreader.IDCardReaderListener _arg1;
_arg1 = com.abc.deviceservice.aidl.normalreader.IDCardReaderListener.Stub.asInterface(data.readStrongBinder());
int _arg2;
_arg2 = data.readInt();
this.idCardReader(_arg0, _arg1, _arg2);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.abc.deviceservice.aidl.normalreader.INormalCard
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
@Override public void normalCardReader(int readMode, com.abc.deviceservice.aidl.normalreader.NormalCardReaderListener listener, int timeout) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(readMode);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
_data.writeInt(timeout);
mRemote.transact(Stub.TRANSACTION_normalCardReader, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void getICCardARQC(int readMode, java.lang.String options, com.abc.deviceservice.aidl.normalreader.ICCardARQCListener listener, int timeout) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(readMode);
_data.writeString(options);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
_data.writeInt(timeout);
mRemote.transact(Stub.TRANSACTION_getICCardARQC, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
//3.4.3.2.	读IC55域信息	

@Override public void writeICCard(java.lang.String content, java.lang.String script, com.abc.deviceservice.aidl.normalreader.WriteICCardListener Listener, int timeout) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(content);
_data.writeString(script);
_data.writeStrongBinder((((Listener!=null))?(Listener.asBinder()):(null)));
_data.writeInt(timeout);
mRemote.transact(Stub.TRANSACTION_writeICCard, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void idCardReader(int readMode, com.abc.deviceservice.aidl.normalreader.IDCardReaderListener listener, int timeout) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(readMode);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
_data.writeInt(timeout);
mRemote.transact(Stub.TRANSACTION_idCardReader, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_normalCardReader = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_getICCardARQC = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_writeICCard = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_idCardReader = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
}
public void normalCardReader(int readMode, com.abc.deviceservice.aidl.normalreader.NormalCardReaderListener listener, int timeout) throws android.os.RemoteException;
public void getICCardARQC(int readMode, java.lang.String options, com.abc.deviceservice.aidl.normalreader.ICCardARQCListener listener, int timeout) throws android.os.RemoteException;
//3.4.3.2.	读IC55域信息	

public void writeICCard(java.lang.String content, java.lang.String script, com.abc.deviceservice.aidl.normalreader.WriteICCardListener Listener, int timeout) throws android.os.RemoteException;
public void idCardReader(int readMode, com.abc.deviceservice.aidl.normalreader.IDCardReaderListener listener, int timeout) throws android.os.RemoteException;
}
