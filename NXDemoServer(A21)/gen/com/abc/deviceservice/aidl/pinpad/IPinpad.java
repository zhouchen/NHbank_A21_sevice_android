/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: E:\\eclipseworkspace\\NXDemoServer(A21)\\src\\com\\abc\\deviceservice\\aidl\\pinpad\\IPinpad.aidl
 */
package com.abc.deviceservice.aidl.pinpad;
public interface IPinpad extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.abc.deviceservice.aidl.pinpad.IPinpad
{
private static final java.lang.String DESCRIPTOR = "com.abc.deviceservice.aidl.pinpad.IPinpad";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.abc.deviceservice.aidl.pinpad.IPinpad interface,
 * generating a proxy if needed.
 */
public static com.abc.deviceservice.aidl.pinpad.IPinpad asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.abc.deviceservice.aidl.pinpad.IPinpad))) {
return ((com.abc.deviceservice.aidl.pinpad.IPinpad)iin);
}
return new com.abc.deviceservice.aidl.pinpad.IPinpad.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_loadMainKey:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
java.lang.String _arg1;
_arg1 = data.readString();
int _arg2;
_arg2 = data.readInt();
int _arg3;
_arg3 = data.readInt();
int _arg4;
_arg4 = data.readInt();
boolean _result = this.loadMainKey(_arg0, _arg1, _arg2, _arg3, _arg4);
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_loadWorkKey:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
int _arg1;
_arg1 = data.readInt();
int _arg2;
_arg2 = data.readInt();
int _arg3;
_arg3 = data.readInt();
int _arg4;
_arg4 = data.readInt();
boolean _result = this.loadWorkKey(_arg0, _arg1, _arg2, _arg3, _arg4);
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_getPin:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
java.lang.String _arg1;
_arg1 = data.readString();
int _arg2;
_arg2 = data.readInt();
int _arg3;
_arg3 = data.readInt();
com.abc.deviceservice.aidl.pinpad.AidlPinpadListener _arg4;
_arg4 = com.abc.deviceservice.aidl.pinpad.AidlPinpadListener.Stub.asInterface(data.readStrongBinder());
this.getPin(_arg0, _arg1, _arg2, _arg3, _arg4);
reply.writeNoException();
return true;
}
case TRANSACTION_getPublicKey:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
java.lang.String _result = this.getPublicKey(_arg0);
reply.writeNoException();
reply.writeString(_result);
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.abc.deviceservice.aidl.pinpad.IPinpad
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
@Override public boolean loadMainKey(java.lang.String publicKeyName, java.lang.String content, int code, int cipherType, int timeOut) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(publicKeyName);
_data.writeString(content);
_data.writeInt(code);
_data.writeInt(cipherType);
_data.writeInt(timeOut);
mRemote.transact(Stub.TRANSACTION_loadMainKey, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
@Override public boolean loadWorkKey(java.lang.String content, int mainKeyCode, int code, int cipherType, int timeOut) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(content);
_data.writeInt(mainKeyCode);
_data.writeInt(code);
_data.writeInt(cipherType);
_data.writeInt(timeOut);
mRemote.transact(Stub.TRANSACTION_loadWorkKey, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
@Override public void getPin(int inputTimes, java.lang.String bankCardNo, int cipherType, int timeOut, com.abc.deviceservice.aidl.pinpad.AidlPinpadListener listener) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(inputTimes);
_data.writeString(bankCardNo);
_data.writeInt(cipherType);
_data.writeInt(timeOut);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_getPin, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public java.lang.String getPublicKey(java.lang.String name) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
java.lang.String _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(name);
mRemote.transact(Stub.TRANSACTION_getPublicKey, _data, _reply, 0);
_reply.readException();
_result = _reply.readString();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
}
static final int TRANSACTION_loadMainKey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_loadWorkKey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_getPin = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_getPublicKey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
}
public boolean loadMainKey(java.lang.String publicKeyName, java.lang.String content, int code, int cipherType, int timeOut) throws android.os.RemoteException;
public boolean loadWorkKey(java.lang.String content, int mainKeyCode, int code, int cipherType, int timeOut) throws android.os.RemoteException;
public void getPin(int inputTimes, java.lang.String bankCardNo, int cipherType, int timeOut, com.abc.deviceservice.aidl.pinpad.AidlPinpadListener listener) throws android.os.RemoteException;
public java.lang.String getPublicKey(java.lang.String name) throws android.os.RemoteException;
}
