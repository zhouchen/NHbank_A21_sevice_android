
package com.abc.deviceservice.aidl;

interface IDeviceService{

void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat,
            double aDouble, String aString);

IBinder getDeviceInfo();
IBinder getPinpad();
IBinder getNormalCard();
IBinder getFingerprint ();
IBinder getSignature();


}