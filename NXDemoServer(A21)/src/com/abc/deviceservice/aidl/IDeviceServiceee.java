package com.abc.deviceservice.aidl;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;
import android.os.RemoteException;
import com.abc.deviceservice.aidl.deviceinfo.DeviceInfoService;
import com.abc.deviceservice.aidl.fingerprint.FingerprintService;
import com.abc.deviceservice.aidl.normalreader.INormalCardService;
import com.abc.deviceservice.aidl.pinpad.PinPadService;
import com.abc.deviceservice.aidl.signature.SignatureService;
import com.joesmate.sdk.util.LogMg;

public class IDeviceServiceee extends Service {

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public void onDestroy() {

		super.onCreate();

	}

	/**
	 * 每次通过startService()方法启动Service时都会被回调。
	 * 
	 * @param intent
	 * @param flags
	 * @param startId
	 * @return
	 */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		return super.onStartCommand(intent, flags, startId);
	}


	@Override
	public void onStart(Intent intent, int startId) {
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	 @Override
	 public void onLowMemory() {
	 super.onLowMemory();
	 LogMg.e("tag", "服务：onLowMemory");
	 }

	 @Override
	 public void onTrimMemory(int level) {
	 super.onTrimMemory(level);
	
	 LogMg.e("tag", "服务：onTrimMemory " + level);
	 }

	 @Override
	 public void onTaskRemoved(Intent rootIntent) {
	 super.onTaskRemoved(rootIntent);
	 LogMg.e("tag", "服务：onTaskRemoved");
	 }
	
	@Override
	public IBinder onBind(Intent arg0) {
		LogMg.e("服务", "绑定服务7777MyBind");
		return new MyBind();
	}

	class MyBind extends IDeviceService.Stub {

		@Override
		public IBinder getPinpad() throws RemoteException {
			return new PinPadService();
		}

		@Override
		public IBinder getDeviceInfo() throws RemoteException {
			return new DeviceInfoService();
		}

		@Override
		public IBinder getNormalCard() throws RemoteException {
			return new INormalCardService();
		}

		@Override
		public IBinder getFingerprint() throws RemoteException {
			return new FingerprintService();
		}

		@Override
		public IBinder getSignature() throws RemoteException {
			return new SignatureService();
		}

		@Override
		public void basicTypes(int anInt, long aLong, boolean aBoolean,
				float aFloat, double aDouble, String aString)
				throws RemoteException {

		}
	}

	// // 设置服务为前台服务
	// /**
	// * id不可设置为0,否则不能设置为前台service
	// */
	// private static final int NOTIFICATION_DOWNLOAD_PROGRESS_ID = 0x0001;
	//
	// private boolean isRemove = false;// 是否需要移除
	//
	// /**
	// * Notification
	// */
	// public void createNotification() {
	// // 使用兼容版本
	// NotificationCompat.Builder builder = new NotificationCompat.Builder(
	// this);
	// // 设置状态栏的通知图标
	// builder.setSmallIcon(R.drawable.ic_launcher);
	// // 设置通知栏横条的图标
	// builder.setLargeIcon(BitmapFactory.decodeResource(getResources(),
	// R.drawable.ic_launcher));
	// // 禁止用户点击删除按钮删除
	// builder.setAutoCancel(false);
	// // 禁止滑动删除
	// builder.setOngoing(true);
	//
	// // 设置通知栏的标题内容
	// builder.setContentTitle("Service!!!");
	// // 创建通知
	// Notification notification = builder.build();
	// // 设置为前台服务
	// startForeground(NOTIFICATION_DOWNLOAD_PROGRESS_ID, notification);
	// }

}
