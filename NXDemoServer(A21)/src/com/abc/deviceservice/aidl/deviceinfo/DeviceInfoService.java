package com.abc.deviceservice.aidl.deviceinfo;

import android.os.RemoteException;

import com.joesmate.sdk.reader.ReaderDev;

/**
 * 设备类
 * 
 * **/
public class DeviceInfoService extends IDeviceInfo.Stub {


	// 连接设备
	@Override
	public int connectDevice() {

		int i = ReaderDev.getInstance().openDevice();
		if (i == 0) {

			return 0;
		} else {
             
		}
		return i;
	}

	// 断开设备
	@Override
	public void disConnectDevice() throws RemoteException {
		int i = ReaderDev.getInstance().closeDevice();
		if (i == 0) {

		} else {
		}

	}

	// 设备序列号
	@Override
	public String getSN() throws RemoteException {
		String SNStr = ReaderDev.getInstance().getSnr();
		return SNStr;

	}

	// 厂商ID
	@Override
	public String getVID() throws RemoteException {
		return "JM20150306";
	}

	// 厂商名称
	@Override
	public String getVName() throws RemoteException {
		return "北京中金银利";
	}

	// 获取终端固件版本
	@Override
	public String getFirmwareVersion() throws RemoteException {
		return "JM-0.0.1";
	}

	// 获取终端型号
	@Override
	public String getDeviceModel() throws RemoteException {
		return "JM-A21";
	}

	// 获取当前设备状态
	@Override
	public boolean getConnectState() throws RemoteException {
		boolean isc = ReaderDev.getInstance().isBTConnected();
		return isc;

	}

	// 取消操作
	@Override
	public void cancelOperation() throws RemoteException {

	}

}
