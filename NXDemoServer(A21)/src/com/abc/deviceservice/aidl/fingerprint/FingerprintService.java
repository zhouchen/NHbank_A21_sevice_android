package com.abc.deviceservice.aidl.fingerprint;

import org.json.JSONObject;

import android.os.RemoteException;
import android.util.Log;

import com.abc.deviceservice.aidl.fingerprint.FingerprintListener;
import com.abc.deviceservice.aidl.fingerprint.IFingerprint;
import com.joesmate.sdk.reader.MT3YCmdMan;
import com.joesmate.sdk.reader.TcFingerDev;
import com.joesmate.sdk.util.LogMg;

public class FingerprintService extends IFingerprint.Stub {
	 //e8 8e b7 e5 8f 96 e5 a4 b1 e8 b4 a5 获取失败
		public void GetSigFialed() {
		
			byte[] cmd = new byte[] { (byte) 0xc0, (byte) 0x0e, (byte) 0xe8,
					(byte) 0x8e, (byte) 0xb7, (byte) 0xe5, (byte) 0x8f,
					(byte) 0x96, (byte) 0xe5, (byte) 0xa4, (byte) 0xb1,
					(byte) 0xe8, (byte) 0xb4, (byte) 0xa5, (byte) 0x00,
					(byte) 0x00, (byte) 0x00 };
			MT3YCmdMan cmdMan = new MT3YCmdMan(cmd);
			 cmdMan.SendRecv();
		}
	@Override
	public void readFinger(int timeout, FingerprintListener listener)
			throws RemoteException {

		TcFingerDev tc = TcFingerDev.getInstance();
		String str = tc.sampFingerPrint(timeout);
        LogMg.e("天诚指纹串口指纹农行固件==",str);
		try {
			if (str.length() > 100) {
				JSONObject json = new JSONObject();
                json.put("vendorCode", "TECHSHINO");
				json.put("data", str);
				listener.onResult(json.toString());
			} else {
				JSONObject json = new JSONObject();
				json.put("code", -1303);
				json.put("message", "读取指纹失败");
				GetSigFialed();
				listener.onError(json.toString());
			}
		} catch (Exception e) {
		}
	}
	// WlFingerDev w=WlFingerDev.getInstance();
	// String str= w.sampFingerPrint(timeout);
	// LogMg.d("指纹信息==", str+"==");
	// if(str.length()>256){
	// listener.onNormalCardReaderResult(str);
	// }else{
	// int code=Integer.parseInt(str);
	// String message= RetCode.GetErrMsg(code);
	// listener.onError(code, message);
	// }
	//
	// }

}
