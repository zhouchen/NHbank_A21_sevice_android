package com.abc.deviceservice.aidl.normalreader;

 interface ICCardARQCListener {
    /**
     * 结果返回
     *@param data:55域信息（TLV格式） jsonobject  string 格式
     */
void onNormalCardReaderResult(String data);
/**
* 读卡失败
* @param error - 错误码
* @param message - 错误描述
*/
void onError(String str);

}