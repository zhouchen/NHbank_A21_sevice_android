package com.abc.deviceservice.aidl.normalreader;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.RemoteException;
import android.util.Log;
import com.joesmate.sdk.reader.EMVTrade;
import com.joesmate.sdk.reader.IDCardDev;
import com.joesmate.sdk.reader.MT3YCmdMan;
import com.joesmate.sdk.reader.ReaderDev;
import com.joesmate.sdk.util.LogMg;
import com.joesmate.sdk.util.ToolFun;

@SuppressLint("DefaultLocale") public class INormalCardService extends INormalCard.Stub {

	ReaderDev reader = ReaderDev.getInstance();
	JSONObject json = new JSONObject();
	@SuppressWarnings("unused")
	@Override
	public void normalCardReader(int readMode,
			NormalCardReaderListener listener, int timeout)
			throws RemoteException {
		/**
		 * 读卡类型 String 1：磁条卡 2：接触IC卡 3：非接IC卡
		 **/

		

		// IC卡
		try {
			if (readMode == 1) {// 磁条卡
				json = EMVTrade.getInstance().readMg(timeout);
				if (json.length() > 2) {

					JSONObject jsonob = new JSONObject();

					String cardno = json.getString("cardNo");
					// 二磁道信息
					String track2Data = json.getString("track2Data");
					Log.e("er磁道信息",track2Data+"");
					// 三磁道信息
					String track3Data = json.getString("track3Data");
					// 序列号
					String serialNumber = "";
					String track1Data = json.getString("track1Data");

					String userName = json.getString("userName");
					String validity = json.getString("validity");
					String documentNumber = "";
					String credentialType = "";

					String atrInfo = "";
					// 当前刷卡类型返回
					String INPUT_TYPE = json.getString("cardtype");
					;
					jsonob.put("cardNo", cardno);
					jsonob.put("track1Data", track1Data);
					jsonob.put("track2Data", track2Data);
					jsonob.put("track3Data", track3Data);
					jsonob.put("serialNumber", serialNumber);
					jsonob.put("userName", userName);
					jsonob.put("validity", validity);
					jsonob.put("documentNumber", documentNumber);
					jsonob.put("credentialType", credentialType);
					jsonob.put("readMode", INPUT_TYPE);
					jsonob.put("atrInfo", atrInfo);
					String data = cardno + track1Data + track2Data + track3Data
							+ serialNumber + userName + validity
							+ documentNumber + credentialType + INPUT_TYPE
							+ atrInfo;
					jsonob.put("data", data);
					ConnectSpeakSuceess();
					listener.onNormalCardReaderResult(jsonob.toString());
				} else {

					EMVTrade.getInstance().ConnectSpeak();
					JSONObject jsons = new JSONObject();
					if(jsons.length()<=0){
						jsons.put("message", "磁条卡读取失败");
						jsons.put("code", "-5");
						listener.onError(jsons.toString());
					}
					String str = (String) json.get("ErrCode");
					
					if (str.equals("-19")) {
						jsons.put("message", json.get("ErrMsg"));
						jsons.put("code", json.get("ErrCode"));
						listener.onError(jsons.toString());
					} else {
						jsons.put("message", json.get("ErrMsg"));
						jsons.put("code", json.get("ErrCode"));
						listener.onError(jsons.toString());
					}

				}

			} else if (readMode == 2) {// 接触卡
				JSONObject js = new JSONObject();
				int o = getIC(json, 0, timeout);
				if(json.length()>2){
				String cardNo = json.getString("cardNo");

				// 二磁道信息
				String track2Data = json.getString("track2");
				// 三磁道信息
				String track3Data = json.getString("track3");
				// 序列号
				String serialNumber = json.getString("serialNumber");
				
				String track1Data = json.getString("track1");
				String userName = json.getString("userName");
				String validity = json.getString("validity");
				String documentNumber = json.getString("documentNumber");
				String credentialType = json.getString("credentialType");

				String atrInfo = "";
				// 当前刷卡类型返回
				String INPUT_TYPE = "2";
				js.put("cardNo", cardNo);
				js.put("track1Data", track1Data);
				js.put("track2Data", track2Data);
				js.put("track3Data", track3Data);
				js.put("serialNumber", serialNumber);
				js.put("userName", userName);
				js.put("validity", validity);
				js.put("documentNumber", documentNumber);
				js.put("credentialType", credentialType);
				js.put("readMode", INPUT_TYPE);
				js.put("atrInfo", atrInfo);
				String data = cardNo + track1Data + track2Data + track3Data
						+ serialNumber + userName + validity
						+ documentNumber + credentialType + INPUT_TYPE
						+ atrInfo;
				js.put("data", data);
				listener.onNormalCardReaderResult(js.toString());
				}else{
					json.put("code", "-1");
					json.put("message", "读取接触式IC卡失败");
					listener.onError(json.toString());
				}
			} else if (readMode == 3) {// 非接触
				JSONObject jso = new JSONObject();				
				int o = getIC(json, 1, timeout);				
				if (o == 0 && json.length() > 3) {				
						
					// Log.e("芯片卡","成功"+json.toString());			
					String cardNo = json.getString("cardNo");	
					
					// 二磁道信息				
					String track2Data = json.getString("track2");	
					// 三磁道信息					
					String track3Data = json.getString("track3");	
					// 序列号				
					String serialNumber = json.getString("serialNumber");		
					String track1Data = json.getString("track1");		
					String userName = json.getString("userName");	
					String validity = json.getString("validity");	
					String documentNumber = json.getString("documentNumber");
					String credentialType = json.getString("credentialType");	
					String atrInfo = "";	
					// 当前刷卡类型返回					
					String INPUT_TYPE = "3";		
					jso.put("cardNo", cardNo);		
					jso.put("track1Data", track1Data);	
					jso.put("track2Data", track2Data);	
					jso.put("track3Data", track3Data);	
					jso.put("serialNumber", serialNumber);	
					jso.put("userName", userName);			
					jso.put("validity", validity);			
					jso.put("documentNumber", documentNumber);	
					jso.put("credentialType", credentialType);	
					jso.put("readMode", INPUT_TYPE);			
					jso.put("atrInfo", atrInfo);				
					String data = cardNo + track1Data + track2Data + track3Data	
							+ serialNumber + userName + validity			
							+ documentNumber + credentialType + INPUT_TYPE	
							+ atrInfo;				
							jso.put("data",data);
					Log.e("非接芯片卡====", "成功" +data);
				listener.onNormalCardReaderResult(jso.toString());
				} else {

					listener.onError(json.toString());
				}
			} else if (readMode == 4) {// 自动
				JSONObject jsonobj = new JSONObject();

				long time1 = System.currentTimeMillis();
				while (System.currentTimeMillis() - time1 < (long) (timeout * 1000)) {
					int e = readTrackData(json);
					if (e == 0) {
						String cardNo = json.getString("cardNo");
						// 二磁道信息
						String track2Data = json.getString("track2");
						// 三磁道信息
						String track3Data = json.getString("track3");
						// 序列号
						String serialNumber = json.getString("serialNumber");
						String track1Data = json.getString("track1");
						;
						String userName = json.getString("userName");
						String validity = json.getString("validity");
						String documentNumber = json
								.getString("documentNumber");
						String credentialType = json
								.getString("credentialType");
						String readICcardType = json.getString("readmode");
						String atrInfo = "";

						jsonobj.put("cardNo", cardNo);
						jsonobj.put("track1Data", track1Data);
						jsonobj.put("track2Data", track2Data);
						jsonobj.put("track3Data", track3Data);
						jsonobj.put("serialNumber", serialNumber);
						jsonobj.put("userName", userName);
						jsonobj.put("validity", validity);
						jsonobj.put("documentNumber", documentNumber);
						jsonobj.put("credentialType", credentialType);
						jsonobj.put("readMode", readICcardType);
						jsonobj.put("atrInfo", atrInfo);
						String data = cardNo + track1Data + track2Data
								+ track3Data + serialNumber + userName
								+ validity + documentNumber + credentialType
								+ readICcardType + atrInfo;
						jsonobj.put("data", data);

						listener.onNormalCardReaderResult(jsonobj.toString());

					} else {
						listener.onError(json.toString());
					}
				}
			}
		} catch (Exception e) {
		}
	}

	/***
	 * 读取IC卡55域信息 readMod: 2：接触IC卡 3：非接IC卡 4：自动 options –标签参数 Listener -回调 见补充说明
	 * 
	 * */
	@Override
	public void getICCardARQC(int readMode, String options,
			ICCardARQCListener listener, int timeout) throws RemoteException {
		short timeOut = (short) timeout;
		JSONObject json=new JSONObject();
		try {
			if (readMode == 2) {
				String[] str = EMVTrade.getInstance().GetARQC(0, "P012000000010000Q012000000000000R003156S006111202T00201U006102550V000",
						"A000000333".toUpperCase(), timeOut);
				if (str[0].equals("0")) {
                    json.put("data", str[1]);
					listener.onNormalCardReaderResult(json.toString());
				} else {
                     json.put("code",str[0]);
                     json.put("message", str[1]);
					listener.onError(json.toString());
				}
			} else if (readMode == 3) {
				String[] str = EMVTrade.getInstance().GetARQC(1, "P012000000010000Q012000000000000R003156S006111202T00201U006102550V000",
						"A000000333".toUpperCase(), timeOut);

				if (str[0].equals("0")) {
					json.put("data", str[1]);
					listener.onNormalCardReaderResult(json.toString());
				} else {
					json.put("code", str[0]);
                    json.put("message", str[1]);
					listener.onError(json.toString());
				}
			} else if (readMode == 4) {
				String[] str = EMVTrade.getInstance().GetARQC(2, options,
						"A000000333".toUpperCase(), timeOut);
				if (str[0].equals("0")) {
					json.put("data", str[1]);
					listener.onNormalCardReaderResult(json.toString());
				} else {
					json.put("code", str[0]);
                    json.put("message", str[1]);
					listener.onError(json.toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void writeICCard(String content, String script,
			WriteICCardListener Listener, int timeout) throws RemoteException {

	}

	@Override
	public void idCardReader(int type, IDCardReaderListener listener,
			int timeout) throws RemoteException {
		final long time1 = System.currentTimeMillis();
		IDCardDev idmessage = IDCardDev.getInstance();
		JSONObject jsonobject = new JSONObject();
		try {
			if (type == 0) {
				int i = idmessage.Read(0, timeout);
				if (i == 0) {
					String fullName = idmessage.getName();
					jsonobject.put("fullName", fullName);
					String identityCardNumber = idmessage.getIDNum();
					jsonobject.put("identityCardNumber", identityCardNumber);
					String gender = idmessage.getSex();
					jsonobject.put("gender", gender);
					String nation = idmessage.getNation();
					jsonobject.put("nation", nation);
					String birthday = idmessage.getBirth();
					jsonobject.put("birthday", birthday);
					String address = idmessage.getAddr();
					jsonobject.put("address", address);
					String organization = idmessage.getGrantDepart();// 颁证机构
					jsonobject.put("organization", organization);
					String effectiveDate = idmessage.getDateStart();
					jsonobject.put("effectiveDate", effectiveDate);
					String expDate = idmessage.getDateEnd();
					jsonobject.put("expDate", expDate);
					Bitmap bi = idmessage.getPhoto();
					String photo = null;
					if (bi != null) {
						photo = ToolFun.bitToStr(idmessage.getPhoto());
						jsonobject.put("photo", photo);
					}else{
					}
					String data = fullName + identityCardNumber + gender
							+ nation + birthday + address + organization
							+ effectiveDate + expDate + photo;
					jsonobject.put("data", data);
					final long time2 = System.currentTimeMillis();
					Log.e("身份证时间",(time2-time1)+"");
					listener.onIDCardReaderResult(jsonobject.toString());
				} else {
					JSONObject json=new JSONObject();
					json.put("code", i);
					json.put("message", "读取身份证失败");
					listener.onError(json.toString());
				}
			} else {
				int i = idmessage.Read(0, timeout);
				if (i == 0) {
					if (idmessage.getID_Type().equals("I")) {
						String fullName = idmessage.getEnName();
						jsonobject.put("fullName", fullName);
						String identityCardNumber = idmessage.getIDNum();
						jsonobject
								.put("identityCardNumber", identityCardNumber);
						String gender = idmessage.getSex();
						jsonobject.put("gender", gender);

						String birthday = idmessage.getBirth();
						jsonobject.put("birthday", birthday);

						String effectiveDate = idmessage.getDateStart();
						jsonobject.put("effectiveDate", effectiveDate);
						String expDate = idmessage.getDateEnd();
						jsonobject.put("expDate", expDate);

						String photo = null;
						if (idmessage.getPhoto() != null) {
							photo = ToolFun.bitToStr(idmessage.getPhoto());
						}
						String data = fullName + identityCardNumber 
								+ gender  + birthday + effectiveDate
								 + expDate  + photo;
						jsonobject.put("data", data);
						listener.onIDCardReaderResult(jsonobject.toString());
					} else {
						JSONObject jsonob = new JSONObject();
						jsonob.put("code", i);
						jsonob.put("message", "读取居住证失败");
						listener.onError(jsonob.toString());
					}

				}
			}
		} catch (Exception e) {
		}
		;

	}

	// 磁条信息
	JSONObject jsons = new JSONObject();

	JSONObject MagCard(int timeout) {
		JSONObject jsonob = new JSONObject();
		try {
			jsonob = EMVTrade.getInstance().readMg(timeout);
			if (jsonob != null) {

				jsons.put("cardNo", jsonob.getString("cardNo"));

				jsons.put("userName", jsonob.getString("userName"));
				// 证件类型

				jsons.put("credentialType", "");
				// 证件号码
				jsons.put("documentNumber", "");

				// 卡序列号
				jsons.put("serialNumber", "");

				// 卡有效期
				jsons.put("validity", jsonob.getString("validity"));
				// 二磁道信息
				jsons.put("track2", jsonob.getString("track2Data"));
				// 一磁道信息
				jsons.put("track1", jsonob.getString("track1Data"));
				// 三磁道信息
				jsons.put("track3", jsonob.getString("track3Data"));

				// ATR信息
				jsons.put("atrInfo", "");
			}
		} catch (Exception e) {
		}
		return jsons;
	}

	/**
	 * code: 值=0，IC 卡接触卡座； 值=1，IC 卡非接触卡座； 值=2，自动；
	 * A 账号
2 B 姓名
3 C 证件类型
4 D 证件号码
5 E 余额
6 F 余额上限
7 G 单笔交易限额
8 H 应用货币类型
9 I 失效日期
10 J IC 卡序列号
11 K 二磁道数据
12 L 一磁道数据
	 * **/
	int getIC(JSONObject json, int code, int timeOut) {
		int il = -1;
		short timeout = (short) timeOut;
		String[] str = EMVTrade.getInstance().GetICCInfo(code, "A000000333",
				"ABCDEFGHIJKL".toUpperCase(), timeout);
		Log.e("接触卡信息23===",str[0]+"===="+str[1]);
		LogMg.e("磁条卡数据==", str[1]);
		try {						
		if (str[0].equals("0")) {			
			il = 0;	
			String string = str[1].substring(4, str[1].length());	
			// 卡号			
			int i = str[1].indexOf("A");	
			String track2 = parseInfo(str[1], "K");	
			if(track2==null){
				String cardNo = str[1].substring(i + 4, str[1].indexOf("B"));
				Log.e("卡号====",cardNo);
				 json.put("cardNo", cardNo.trim());	
			}else{
				int is = track2.indexOf("D");	
				if (is != -1) {						
					String str2 = track2.substring(0, is);
					Log.e("卡号",str2);
					json.put("cardNo", str2.trim());
					json.put("track2", track2.trim());
					}		
			}
				
			
					
			// 姓名			
			String userName = parseInfo(string, "B").trim();
			if(userName.equals("/")){
				json.put("userName", "");	
			}else if(userName.length()>1){
				json.put("userName", userName);
			}else if(userName.length()<=0){
				json.put("userName", "");
			}
			
				
			// 证件类型			
			String credentialType = parseInfo(string, "C");
			json.put("credentialType", credentialType.trim());	
			// 证件号码				
			String documentNumber = parseInfo(string, "D");	
//			if(documentNumber.subSequence(0, 6).equals("202020")){
//				json.put("documentNumber", "");
//			}else if(documentNumber!=null){
//				json.put("documentNumber", ToolFun.hexStr2Str(documentNumber));
//			}
			json.put("documentNumber", ToolFun.hexStr2Str(documentNumber));
			// 卡序列号			
			
			String serialNumber = parseInfo(string, "J");	
			json.put("serialNumber", serialNumber.trim());	
			// 卡有效期				
			String validity = parseInfo(string, "I");
			json.put("validity", validity.trim());		
			// 二磁道信息			
//			String track2Data = parseInfo(str[1], "K");	
////			if (track2Data != null) {				
////				int is = track2Data.indexOf("D");	
////				if (is != -1) {						
////					String str2 = track2Data.substring(0, is);	
////					Log.e("卡号=====", str2 + "");			
////					json.put("cardNo", str2.trim());		
////					}		
////				}		
//			json.put("track2", track2Data.trim());	
			// 一磁道信息			
			String track1Data = parseInfo(str[1], "L").trim();	
			if(track1Data==null){
				json.put("track1","");	
			}else{
				json.put("track1", ToolFun.hexStr2Str(track1Data));
			}
		
			// 三磁道信息				
			json.put("track3", "".trim());	
			
		// ATR信息			
					
		json.put("atrInfo", "".trim());		
		return il;
//		try {
//			
//			if (str[0].equals("0")) {
//				il = 0;
//				// 卡号
//				int i = str[1].indexOf("A");
//				
//				String string = str[1].substring(4, str[1].length());
//				int ii=str[1].indexOf("B");
//				String cardNo=str[1].substring(i+4, ii);
//				Log.e("卡号==",cardNo);
//				 json.put("cardNo", cardNo.trim());
//				// 姓名
//
//				String userName = parseInfo(string, "B").trim();
//				Log.e("姓名==",userName);
//				if(userName.equals("/")){
//					
//					json.put("userName", "");	
//				}else{
//			
//				json.put("userName", userName);
//				}
//				// 证件类型
//
//				String credentialType = parseInfo(string, "C");
//				Log.e("证件类型==",credentialType);
//				json.put("credentialType", credentialType.trim());
//				// 证件号码
//				String documentNumber =( parseInfo(string, "D")).trim();
//				if(documentNumber.substring(0, 6).equals("202020")){
//					json.put("documentNumber","");
//					Log.e("证件号码程度",documentNumber.length()+"=========="+documentNumber);
//				}else{
//					Log.e("证件号码程度======",documentNumber);
//					json.put("documentNumber",ToolFun.hexStr2Str(documentNumber));
//				}
//				
//				
//                
//				// 卡序列号
//				String serialNumber = parseInfo(string, "J");
//				Log.e("卡序列号==",serialNumber);
//				json.put("serialNumber", serialNumber.trim());
//
//				// 卡有效期
//				String validity = parseInfo(string, "I");
//				Log.e("卡序列号==",validity);
//				json.put("validity", validity.trim());
//				// 二磁道信息
//				String track2Data = parseInfo(str[1], "K");
//				if (track2Data != null) {
//					int is = track2Data.indexOf("D");
//					if (is != -1) {
//						String str2 = track2Data.substring(0, is);
//						json.put("cardNo", str2.trim());
//					}
//
//				}
//				Log.e("卡序列号==",track2Data);
//				json.put("track2", track2Data.trim());
//				// 一磁道信息
//				String track1Data = parseInfo(str[1], "L").trim();
//				Log.e("卡序列号==",track1Data);
//				json.put("track1", ToolFun.hexStr2Str(track1Data));
//				// 三磁道信息
//				json.put("track3", "".trim());
//
//				// ATR信息
//				json.put("atrInfo", "".trim());
//				return il;
			} else {
				il = Integer.parseInt(str[0]);
				json.put("code", "-1");
				json.put("message", "读取银行卡失败");

				return il;
			}
		} catch (Exception e) {
		}
		return il;
	}

	protected int readTrackData(JSONObject jsonobj) {

		int st = 0;
		try {

			jsonobj = MagCard(0);

			if (jsonobj.length() > 3) {
				return 0;
			}
			st = getIC(jsonobj, 0, 0);// ic
			if (st == 0) {
				return st;
			}
			st = getIC(jsonobj, 1, 0);// 非接
			if (st == 0) {
				return st;
			}
		} catch (Exception e) {
		}
		return st == 0 ? st : -76;
	}

	// 数据解析
	String parseInfo(String stringData, String str) {
		String credentialType = null;
		if (str != null) {
			String s = null;
			int i = stringData.indexOf(str);
			s = stringData.substring(i + 2, i + 4);
			int si = Integer.parseInt(s);
			credentialType = stringData.substring(i + 4, i + si * 2 + 4);
		}
		return credentialType;
	}

	// 获取55域
	int get55(int mode, String string, JSONObject jsonobj, int timeOut) {
		short timeout = (short) timeOut;
		String[] str = EMVTrade.getInstance().GetARQC(mode, string,
				"A000000333".toUpperCase(), timeout);
		try {
			if (str[0].equals("0")) {
				jsonobj.put("data", str[1]);
			} else {
				jsonobj.put("message", str[1]);
				jsonobj.put("code", str[0]);
			}
		} catch (Exception e) {
		}
		return Integer.parseInt(str[0]);
	}

	// 读取磁条卡语音
	public void ConnectSpeak() {
		byte[] cmd = new byte[] { (byte) 0xc0, (byte) 0x0e, (byte) 0xe8,
				(byte) 0xaf, (byte) 0xbb, (byte) 0xe5, (byte) 0x8d,
				(byte) 0xa1, (byte) 0xe5, (byte) 0xa4, (byte) 0xb1,
				(byte) 0xe8, (byte) 0xb4, (byte) 0xa5, (byte) 0x00,
				(byte) 0x00, (byte) 0x00 };
		MT3YCmdMan cmdMan = new MT3YCmdMan(cmd);
		 cmdMan.SendRecv();
	}

	// 读卡成功 e8 af bb e5 8d a1 e6 88 90 e5 8a 9f
	public void ConnectSpeakSuceess() {
		
		byte[] cmd = new byte[] { (byte) 0xc0, (byte) 0x0e, (byte) 0xe8,
				(byte) 0xaf, (byte) 0xbb, (byte) 0xe5, (byte) 0x8d,
				(byte) 0xa1, (byte) 0xe6, (byte) 0x88, (byte) 0x90,
				(byte) 0xe5, (byte) 0x8a, (byte) 0x9f, (byte) 0x00,
				(byte) 0x00, (byte) 0x00 };
		MT3YCmdMan cmdMan = new MT3YCmdMan(cmd);
	    cmdMan.SendRecv();
	}
}
