package com.abc.deviceservice.aidl.pinpad;

import org.json.JSONObject;

import com.abc.deviceservice.aidl.pinpad.AidlPinpadListener;
import com.abc.deviceservice.aidl.pinpad.IPinpad;
import com.joesmate.sccba.SccbaReader;
import com.joesmate.sdk.reader.KeyboardDev;
import com.joesmate.sdk.util.ToolFun;

import android.os.RemoteException;
import android.util.Log;

public class PinPadService extends IPinpad.Stub {
 boolean ispin=false;
	@Override
	public boolean loadMainKey(String publicKeyName, String content, int code,
			int cipherType, int timeOut) throws RemoteException {
		int i = KeyboardDev.getInstance().UpdateMKey(0,	16,
				"89A2E53D0E4A7A02DA94F215D6A11502");
		Log.e("下载主密钥返回值",i+"");
		if(i==0){
			
			return true;
		}else{
			return false;	
		}
		
	}

	@Override
	public boolean loadWorkKey(String content, int mainKeyCode, int code,
			int cipherType, int timeOut) throws RemoteException {
		int i = KeyboardDev.getInstance().DownLoadWKey(0, 0, 16,
				"30E242ACABE9A93E427174D168C33F0A");
		Log.e("下载工作密钥返回值",i+"");
		if(i==0){
			
			return true;
		}else{
			return false;	
		}
	}

	@Override
	public String getPublicKey(String name) throws RemoteException {
		return "";
	}

	//
	@Override
	public void getPin(int inputTimes, String bankCardNo, int cipherType,
			int timeOut, AidlPinpadListener listener) throws RemoteException {
		try{
			JSONObject json=new JSONObject();
		byte[] pin = KeyboardDev.getInstance().getPassword(0, 6,
				"0001234567890123", 30);
		
		if(pin.length>7){
			
			json.put("data", ToolFun.printHexString(pin));
			listener.onResult(json.toString());
		}else{
			json.put("code", "-1");
			json.put("message", "获取密码失败");
			listener.onError(json.toString());
		}
		}catch(Exception e){
			
		}
	}
}
