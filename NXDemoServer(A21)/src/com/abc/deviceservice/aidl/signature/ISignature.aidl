package com.abc.deviceservice.aidl.signature;
import com.abc.deviceservice.aidl.signature.SigntrueListener;

interface ISignature{

void openSignature(int width,int height,SigntrueListener listener);
void  getSignature (SigntrueListener listener);
void clearSignature (SigntrueListener listener);
void closeSignature (SigntrueListener listener);

}