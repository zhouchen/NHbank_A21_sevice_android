package com.abc.deviceservice.aidl.signature;

import org.json.JSONException;
import org.json.JSONObject;
import com.abc.deviceservice.aidl.signature.ISignature;
import com.abc.deviceservice.aidl.signature.SigntrueListener;
import com.joesmate.sdk.reader.MT3YCmdMan;
import com.joesmate.sdk.reader.SignatureDataMan;
import com.joesmate.sdk.reader.SignatureDev;

import android.os.RemoteException;
import android.util.Log;

public class SignatureService extends ISignature.Stub {
	JSONObject json = new JSONObject();
    SignatureDev si=SignatureDev.getInstance();

	@Override
	public void openSignature(int width, int height, SigntrueListener listener)
			throws RemoteException {
		final byte cmd[] = new byte[] { (byte) 0xc7, (byte) 0x01 };
		SignatureDataMan sd = new SignatureDataMan(cmd, 60*1000);
		int iRet = sd.execCmd();
		Log.e("打开签名屏",iRet+"");
		  try {
			
			if (iRet == 0) {
				json.put("data", "打开电磁屏");
				
				listener.onResult(json.toString());
			} else {
				
				json.put("message", "打开电磁屏失败");
				json.put("code", iRet);
				listener.onError(json.toString());
			}
		      } catch (Exception e) {
		     }
		 
	}

	@Override
	public void clearSignature(SigntrueListener listener)
			throws RemoteException {
		final byte cmd[] = new byte[] { (byte) 0xc7, (byte) 0x03 };
		SignatureDataMan sd = new SignatureDataMan(cmd, 30 * 1000);
		int iRet = sd.execCmd();
		try {
			if (iRet == 0) {
				json.put("data", "清除成功");
				listener.onResult(json.toString());
			} else {
				json.put("code", iRet);
				json.put("message", "清除失败");
				listener.onError(json.toString());
			}
		} catch (Exception e) {
		}
	}

	@Override
	public void closeSignature(SigntrueListener listener)
			throws RemoteException {
		final byte cmd[] = new byte[] { (byte) 0xc7, (byte) 0x02 };
		SignatureDataMan sd = new SignatureDataMan(cmd, 30 * 1000);
		int iRet = sd.execCmd();
		try {
			if (iRet == 0) {
				json.put("data", "关闭成功");
				listener.onResult(json.toString());
			} else {
				json.put("code", iRet);
				json.put("message", "关闭失败");
				listener.onError(json.toString());
			}
		} catch (Exception e) {
		}
	}

	@Override
	public void getSignature(SigntrueListener listener) throws RemoteException {
		final byte cmd[] = new byte[] { (byte) 0xc7, (byte) 0x06 };
		SignatureDataMan sd = new SignatureDataMan(cmd, 100 * 1000);
		int iRet = sd.execCmd();
	 Log.e("获取签名屏数据返回值",iRet+"");
		try {
			if (iRet == 0) {
				json = sd.getResult();
				String mSingStr = (String) json.get("SignaBMP");
				json.put("data", mSingStr);
				
				listener.onResult(json.toString());
			} else {
				json.put("code", iRet);
				json.put("message", "签字数据获取失败");				
				listener.onError(json.toString());
			}
		} catch (JSONException e) {
		}
		
	}

}
