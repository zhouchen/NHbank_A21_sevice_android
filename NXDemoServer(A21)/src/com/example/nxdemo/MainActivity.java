package com.example.nxdemo;



import org.json.JSONObject;


import com.abc.deviceservice.aidl.IDeviceService;
import com.abc.deviceservice.aidl.R;
import com.abc.deviceservice.aidl.deviceinfo.IDeviceInfo;
import com.abc.deviceservice.aidl.fingerprint.FingerprintListener;
import com.abc.deviceservice.aidl.fingerprint.IFingerprint;
import com.abc.deviceservice.aidl.normalreader.ICCardARQCListener;
import com.abc.deviceservice.aidl.normalreader.IDCardReaderListener;
import com.abc.deviceservice.aidl.normalreader.INormalCard;
import com.abc.deviceservice.aidl.normalreader.NormalCardReaderListener;
import com.abc.deviceservice.aidl.signature.ISignature;
import com.abc.deviceservice.aidl.signature.SigntrueListener;
import com.joesmate.sdk.reader.EMVTrade;
import com.joesmate.sdk.reader.KeyboardDev;
import com.joesmate.sdk.reader.ReaderDev;
import com.joesmate.sdk.reader.WlFingerDev;
import com.joesmate.sdk.util.LogMg;
import com.joesmate.sdk.util.ToolFun;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	final static int MSG_FAILED = 1;
	final static int MSG_SUCCESS = 0;
	Intent intent;
	MyConn conn;
	TextView text;
	ImageView image;
	IDeviceService idInfo;// 设备服务类

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		text = (TextView) findViewById(R.id.textView1);
		image = (ImageView) findViewById(R.id.image);
		intent = new Intent();
		intent.setAction("com.abc.device_service");
		intent.setPackage("com.abc.deviceservice.aidl");
		conn = new MyConn();
		clearData();
		bindService(intent, conn, BIND_AUTO_CREATE);
	}

	public void clearData() {
		text.setText("");
		image.setImageBitmap(null);
	}

	// 绑定服务
	private class MyConn implements ServiceConnection {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			idInfo = IDeviceService.Stub.asInterface(service);
			if (idInfo != null) {
				try {
					IDeviceInfo id = IDeviceInfo.Stub.asInterface(idInfo
							.getDeviceInfo());
					Toast.makeText(MainActivity.this, "服务开启", Toast.LENGTH_LONG)
							.show();
				} catch (RemoteException e) {
				}
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Toast.makeText(MainActivity.this, "断开成功", Toast.LENGTH_LONG).show();
		}

	}
public void WlFinger(View view){
	String s=WlFingerDev.getInstance().sampFingerPrint(30);
	text.setText("指纹数据:\n\r" + s);
}
	// 指纹仪
	public void GetFinger(View view) {
		clearData();
//		String string=WlFingerDev.getInstance().sampFingerPrint(20);
//		text.setText("指纹数据:\n\r" + string);
		try {
			IFingerprint finger = IFingerprint.Stub.asInterface(idInfo
					.getFingerprint());
			finger.readFinger(20, new FingerprintListener.Stub() {

				@Override
				public void onResult(String data) throws RemoteException {
					Log.e("指纹===",data);
					try{
						JSONObject json=new JSONObject(data);
						String string=(String)json.getString("data");
						text.setText("指纹数据:\n\r" + string);
					}catch(Exception e){}
					
				}

				@Override
				public void onError(String Errmessage) throws RemoteException {
					try{
						JSONObject json=new JSONObject(Errmessage);
						String string=(String)json.getString("message");
						String code=(String)json.getString("code");
						text.setText("指纹数据失败:\n\r" + string+"\r\n"+code);
					}catch
					(Exception e){}
					

				}
			});

		} catch (Exception e) {
		}

	}

	// //非接触式ic卡
	/**
	 * 
	 * 
	 * **/
	public void NFCIC(View view) {
		clearData();
		try {
//			String[] str = EMVTrade.getInstance().GetICCInfo(1, "A000000333",
//			"ABCDEFGHIJKL".toUpperCase(), (short)20);
//	if(str[0].equals("0")){
//		
//		text.setText("非接触卡IC卡读取成功：\r\n"+str[1]);
//	}else{
//		text.setText("非接触卡IC卡读取失败：\r\n"+str[1]+str[0]);
//	}
			INormalCard is = INormalCard.Stub.asInterface(idInfo
					.getNormalCard());
			is.normalCardReader(3, new NormalCardReaderListener.Stub() {

				@Override
				public void onNormalCardReaderResult(String data)
						throws RemoteException {
					Log.e("feijieka非接卡==",data);
					try{
						JSONObject json=new JSONObject(data);
						String str="卡号:"+json.getString("cardNo")+"\r\n"+
						           "二磁道："+json.getString("track2Data")+"\r\n"+
						           "卡序列号："+json.getString("serialNumber")+"\r\n"+
						           "姓名:"+json.getString("userName")+"\r\n"+
						           "证件号码:"+json.getString("documentNumber")+"\r\n";
						text.setText("非接银行卡读取成功：\n\r"+str);
					}catch
					(Exception e){
						e.printStackTrace();
					}
					
				}

				@Override
				public void onError(String message) throws RemoteException {
					try{
						JSONObject json=new JSONObject(message);
						String string=(String)json.getString("message");
						String code=(String)json.getString("code");
						text.setText("非接银行卡读取失败:\n\r"+string+"\r\n"+code);
					}catch(Exception e){e.printStackTrace();}
				}
			}, 20);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 非接触式55卡
	public void NFC55(View view) {
		try {
			INormalCard is = INormalCard.Stub.asInterface(idInfo
					.getNormalCard());
			is.getICCardARQC(
					3,
					"P012000000010000Q012000000000000R003156S006111202T00201U006102550V000",
					new ICCardARQCListener.Stub() {

						@Override
						public void onNormalCardReaderResult(String data)
								throws RemoteException {
							// TODO Auto-generated method stub
							text.setText(data);
						}

						@Override
						public void onError(String message)
								throws RemoteException {
							// TODO Auto-generated method stub
							text.setText("55yu===" + message);
						}
					}, 30);
		} catch (Exception e) {
		}
		
	}

	// 磁条卡
	public void GetMag(View view) {
		clearData();
		try {
//			JSONObject jsonob = new JSONObject();
//			jsonob=	EMVTrade.getInstance().readMg(30);
//			Log.e("次都快返回",jsonob.length()+"");
//			if(jsonob.length()>2){
//				String cardno = jsonob.getString("cardNo");
//				// 二磁道信息
//				String track2Data = jsonob.getString("track2Data");
//				// 三磁道信息
//				String track3Data = jsonob.getString("track3Data");
//				text.setText("磁条卡读取成功：\n\r"+"卡号："+cardno+"\n\r二磁道信息："+track2Data+"\n\r三磁道信息："+track3Data);
//			}else{
//				text.setText("磁条卡读取失败");
//			}
			INormalCard in = INormalCard.Stub.asInterface(idInfo
					.getNormalCard());
			in.normalCardReader(1, new NormalCardReaderListener.Stub() {
				@Override
				public void onNormalCardReaderResult(String data)
						throws RemoteException {
					try{
						JSONObject json=new JSONObject(data);
						String cardno=json.getString("cardNo");
					    String track2=json.getString("track2Data");
					    String track3=json.getString("track3Data");
					    String track1=json.getString("track1Data");
						text.setText("磁条卡读取成功：\r\n卡号："+cardno+
								               "\n\r一磁道信息："+track1+
								                "\n\r二磁道信息："+track2+
								                "\n\r三磁道信息："+track3);
					}catch
					(Exception e){}
				}

				@Override
				public void onError(String message) throws RemoteException {
					try{
						Log.e("辞掉卡失败",message);
						JSONObject json=new JSONObject(message);
						String string=(String)json.getString("message");
						String code=(String)json.getString("code");
						text.setText("磁条卡读取失败返回:\r\n"+string+"\r\n"+code);
					}catch(Exception e){}
				}
			}, 20);
		} catch (Exception e) {
		}
	}

	public void Connect(View view) {
		LogMg.e("指纹数据===","");
		LogMg.i("身份证信息==","");
		try {
			int i = ReaderDev.getInstance().openDevice();
		
			if (i == 0) {

				text.setText("连接成功" );
			} else {

				text.setText("连接失败" );
			}
		} catch (Exception e) {
		}

	}

	// 身份证
	public void GetID(View view) {
		clearData();

		try {
			INormalCard in = INormalCard.Stub.asInterface(idInfo
					.getNormalCard());
			in.idCardReader(0, new IDCardReaderListener.Stub() {

				@Override
				public void onIDCardReaderResult(String data)
						throws RemoteException {
					try {
						JSONObject json=new JSONObject(data);
						String str=json.getString("fullName");//姓名
						String sex=json.getString("gender");//性别
						String identityCardNumber=json.getString("identityCardNumber");//身份证号
						String nation=json.getString("nation");//民族
						String birthday=json.getString("birthday");//出生日期
						String biryear=birthday.substring(0, 4);
						String birmouth=birthday.substring(4,6);
						String birday=birthday.substring(6,8);
						String bir=biryear+"."+birmouth+"."+birday;
						String address=json.getString("address");//地址
						String organization=json.getString("organization");//发证机构
						String datestr=json.getString("effectiveDate");//有效期开始时间
						String datayear=datestr.substring(0, 4);
						String datamouth=datestr.substring(4, 6);
						String dataday=datestr.substring(6,8);
						String startDate=datayear+"."+datamouth+"."+dataday;
						String enddate=json.getString("expDate");
						String endbir=null;
						if(enddate.length()>2){
							String endyear = enddate.substring(0, 4);
							String endmoutch = enddate.substring(4, 6);
							String endday = enddate.substring(6, 8);
							 endbir = endyear + "." + endmoutch + "."
									+ endday;
						}else{
							endbir=enddate;
						}
						String ima=json.getString("photo");
						text.setText("\n\t姓名："+str+"\n\t性别："+sex+"\n\t民族："+nation+"族\n\t身份证号码："+
						                      identityCardNumber+"\n\t出生日期："+bir+"\n\t地址："+address+
						                      "\n\t签发机关："+organization+"\n\t有效期："+startDate+"-"+endbir);
						
						Bitmap bit=	stringToBitmap(ima);
						image.setImageBitmap(bit);
					} catch (Exception e) {
					}
				}

				@Override
				public void onError(String errmessage) throws RemoteException {
					try{
						text.setText(errmessage);
						JSONObject json=new JSONObject(errmessage);
						String string=(String)json.getString("message");
						String code=(String)json.getString("code");
						text.setText("身份证读取失败:\n\r"+string+"\r\n"+code);
					}catch(Exception e){}
				}
			}, 30);
		} catch (Exception e) {
		}
	}

	// 接触式IC卡
	public void GetIC(View view) {
            clearData();
//		
//			String[] str = EMVTrade.getInstance().GetICCInfo(0, "A000000333",
//					"ABCDEFGHIJKL".toUpperCase(), (short)20);
//			Log.e("接触卡信息23===",str[0]+"===="+str[1]);
//			
//			if(str[0].equals("0")){
//				text.setText("接触卡IC卡读取成功：\r\n"+str[1]);
//				
//			}else{   
//				text.setText("接触卡IC卡读取失败：\r\n"+str[1]+str[0]);
//			}
			try {
//			INormalCard is = INormalCard.Stub.asInterface(idInfo
//					.getNormalCard());
//			is.normalCardReader(2, new NormalCardReaderListener.Stub() {
//
//				@Override
//				public void onNormalCardReaderResult(String data)
//						throws RemoteException {
//					try{
//						JSONObject json=new JSONObject(data);
//						String str="卡号:"+json.getString("cardNo")+"\r\n"+
//						           "二磁道信息："+json.getString("track2Data")+"\r\n"+
//						           "卡序列号："+json.getString("serialNumber")+"\r\n"+
//						           "姓名:"+json.getString("userName")+"\r\n"+
//						           "证件号码:"+json.getString("documentNumber")+"\r\n";
//					    
//						text.setText("接触卡IC卡读取成功：\r\n"+str);
//					}catch
//					(Exception e){}
//				}
//
//				@Override
//				public void onError(String message) throws RemoteException {
//					try{
//						JSONObject json=new JSONObject(message);
//						String string=(String)json.getString("message");
//						String code=(String)json.getString("code");
//						text.setText("接触卡IC卡读取失败:\n\r"+string+"\r\n"+code);
//					}catch(Exception e){}
//				}
//			}, 20);
		} catch (Exception e) {
		}

	}

	// 接触式55域
	public void get55(View view) {
		clearData();
		try {
			INormalCard is = INormalCard.Stub.asInterface(idInfo
					.getNormalCard());
			is.getICCardARQC(
					2,
					"P012000000010000Q012000000000000R003156S006111202T00201U006102550V000",
					new ICCardARQCListener.Stub() {

						@Override
						public void onNormalCardReaderResult(String data)
								throws RemoteException {
							// TODO Auto-generated method stub
							text.setText(data);
						}

						@Override
						public void onError(String message)
								throws RemoteException {
							// TODO Auto-generated method stub
							text.setText("55yu===" + message);
						}
					}, 30);
		} catch (Exception e) {
		}
	}

	// public void NFCIC(View view){
	// short timeout = (short) 60;
	// String[] str = EMVTrade.getInstance().GetICCInfo(1, "A000000333",
	// "ABCDEFGHIJKL".toUpperCase(), timeout);
	// Log.e("sfeif非接",str.toString());
	// if(str[0].equals("0")){
	// text.setText("非接触式IC卡"+str[1]+"");
	// }else{
	//
	// text.setText("非接触式IC卡失败"+str[1]);
	// }
	// }
public void downSM4Main(View view){
	clearData();
	int i = KeyboardDev.getInstance().UpdateSM4MKey(0, 16,
			"22222222222222222222222222222222");
	if(i==0){
		text.setText("下载SM4主密钥成功" + i);
	}else{
		text.setText("下载SM4主密钥失败" + i);
	}	
	}
    public void downSM4work(View view){
    	clearData();
		int i = KeyboardDev.getInstance().DownLoadSM4WKey(0, 0, 16,
				"09F7815DA24FEFC4A6B8D09FE03D8DDE");
		if(i==0){
			text.setText("下载SM4工作密钥成功" + i);
		}else{
		text.setText("下载SM4工作密钥失败" + i);
		}
	}
    public void GetPinPail(View view) {
    	clearData();
    	byte[] pin = KeyboardDev.getInstance().getPasswordpail(20);
		if(pin.length>4){
			text.setText("获取pin明文成功" + ToolFun.printHexString(pin));
		}else{
		text.setText("获取pin明文失败" );
		}
    }
	// 下载主密钥
	public void downMain(View view) {
		clearData();
		int i = KeyboardDev.getInstance().UpdateMKey(0, 16,
				"22222222222222222222222222222222");
		if(i==0){
			text.setText("下载主密钥成功" + i);
		}else{
			text.setText("下载主密钥失败" + i);
		}
		
	}
	//pSAM
	public void Getpsam(View view){
		short timeout = (short) 30;
		String[] str = EMVTrade.getInstance().GetPSAM(0, "A000000333",
				"ABCDEFGHIJKL".toUpperCase(), timeout);
		if(str[0].equals("0")){
			text.setText("PSAM卡成功:" + str[1]);
		}else{
			text.setText("PSAM卡失败:" + str[1]);
		}
	}

	// 下载工作密钥
	public void downwork(View view) {
		clearData();
		int i = KeyboardDev.getInstance().DownLoadWKey(0, 0, 16,
				"08024FCF811DA67208024FCF811DA672");
		if(i==0){
			text.setText("下载工作密钥成功" + i);
		}else{
		text.setText("下载工作密钥失败" + i);
		}
	}
	
	// 获取pin
	public void getpin(View view) {
		clearData();
		byte[] pin = KeyboardDev.getInstance().getPassword(0, 6,
				"0000000000000000", 30);
        if(pin.length>7){
        	text.setText("获取pin成功:" + ToolFun.printHexString(pin));
        }else{
        	text.setText("获取pin失败:" + ToolFun.printHexString(pin));
        }
		

	}

	// 打开签名数据
	public void OpenSign(View view) {
	
		try {
			ISignature is = ISignature.Stub.asInterface(idInfo.getSignature());
			is.openSignature(200, 200, new SigntrueListener.Stub() {

				@Override
				public void onResult(String data) throws RemoteException {
					text.setText("打开签字屏成功");
					
				}

				@Override
				public void onError(String message) throws RemoteException {
					// TODO Auto-generated method stub
					text.setText("打开签字屏失败");
				}
			});
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// 获取签名数据
	public void getSign(View view) {
		clearData();
		try {
			ISignature is = ISignature.Stub.asInterface(idInfo.getSignature());
			is.getSignature(new SigntrueListener.Stub() {

				@Override
				public void onResult(String data) throws RemoteException {
					try {
						JSONObject json = new JSONObject(data);
						String dat = json.getString("data");
						Bitmap bit = stringToBitmap(dat);
						image.setImageBitmap(bit);
						text.setText("获取签名数据成功");
					} catch (Exception e) {
					}

				}

				@Override
				public void onError(String message) throws RemoteException {
					text.setText("获取签名数据失败");
				}
			});
		} catch (Exception e) {
		}
	}
	
	// 重新签名
	public void clearSign(View v) {
		clearData();
		try {
			ISignature is = ISignature.Stub.asInterface(idInfo.getSignature());
			is.clearSignature(new SigntrueListener.Stub() {

				@Override
				public void onResult(String data) throws RemoteException {
					// TODO Auto-generated method stub
					text.setText(data);
				}

				@Override
				public void onError(String message) throws RemoteException {
					// TODO Auto-generated method stub
					text.setText(message);
				}
			});
		} catch (Exception e) {
		}
	}

	// 关闭签名
	public void closeSign(View v) {
		clearData();
		try {
			ISignature is = ISignature.Stub.asInterface(idInfo.getSignature());
		     is.closeSignature(new SigntrueListener.Stub() {
				
				@Override
				public void onResult(String data) throws RemoteException {
					// TODO Auto-generated method stub
					text.setText(data);
				}
				
				@Override
				public void onError(String message) throws RemoteException {
					// TODO Auto-generated method stub
					text.setText(message);
				}
			});
		} catch (Exception e) {
		}
	}

	public Bitmap stringToBitmap(String string) {
		// 将字符串转换成Bitmap类型
		Bitmap bitmap = null;
		try {
			byte[] bitmapArray;
			bitmapArray = Base64.decode(string, Base64.DEFAULT);
			bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0,
					bitmapArray.length);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bitmap;
	}
	
	
	public void getSM4pin(View view){
		byte[] b=KeyboardDev.getInstance().getSM4Password(0, 6,
				"0000000000000000", 30);
		Log.e("============",ToolFun.printHexString(b));
		text.setText(ToolFun.printHexString(b));
	}
	
}
