package com.example.ymreader;

/*     */

/*     */

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.joesmate.sdk.util.CmdCode;
import com.joesmate.sdk.util.LogMg;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

public abstract class CommManagement extends Thread {
	protected static CommManagement sInstance;
	private BluetoothAdapter _bluetooth = BluetoothAdapter.getDefaultAdapter();
	protected BluetoothSocket socket = null;
	protected BluetoothDevice mLastDevice = null;
	protected InputStream mInputStream = null;
	protected OutputStream mOutputStream = null;
	protected volatile boolean mIsConnected = false;
	private volatile boolean mThreadPause = true;
	private volatile boolean mThreadExit = false;
	private static int NconnectType = 1;
	protected String mVersion = "";
	protected int ulTotalTimeOuts = 8000;
	public boolean isBigData = false;

	public void setBigData(boolean isbigData) {
		this.isBigData = isbigData;
	}

	protected CommManagement() {
	}

	public void run() {
		// LogMg.i("CommManagement", this + " runing...");

		while (true) {
			while (true) {
				this.delay(10L);
				if (this.mThreadExit) {
					if (this.mIsConnected) {
						this.bluetoothClose();
					}

					// LogMg.i("CommManagement", this + " exited");
					return;
				}

				if (this.mThreadPause) {
					if (this.mIsConnected) {
						this.bluetoothClose();
					}

					this.delay(1000L);
				} else {
					if (!this.mIsConnected) {
						try {
							if (this.deviceConnect(mLastDevice) != 0) {
								this.delay(1500L);
								continue;
							}
						} catch (Exception var2) {
							var2.printStackTrace();
						}
					}

					this.delay(10000L);
					this.heartBeat();
				}
			}
		}
	}

	private int heartBeat() {
		synchronized (CommManagement.class) {
			byte[] Writebuffer = new byte[] { (byte) 88 };
			if (this.socket != null && this.socket.isConnected()) {
				try {
					this.mOutputStream.write(Writebuffer, 0, 1);
					this.mOutputStream.flush();
				} catch (IOException var4) {
					this.bluetoothBroken();
					var4.printStackTrace();
					return -17;
				}

				return 0;
			} else {
				this.bluetoothBroken();
				return -17;
			}
		}
	}

	protected int ReadVersion() {

		synchronized (CommManagement.class) {
			byte[] cmd = new byte[] { (byte) 49, (byte) 17, (byte) 0 };
			byte[] recv = new byte[CmdCode.MAX_SIZE_BUFFER];
			int[] recvLen = new int[1];
			int st = this.SendRecv(cmd, cmd.length, recv, recvLen);
			if (st == 0) {
				this.mVersion = new String(recv);
			} else {
				this.mVersion = "";
			}

			return st;
		}
	}

	private void delay(long m) {
		try {
			Thread.sleep(m);
		} catch (Exception var4) {
			this.bluetoothClose();
		}

	}

	public int bluetoothClose() {

		synchronized (CommManagement.class) {
			this.mVersion = "";
			this.mIsConnected = false;
			if (this.mInputStream != null) {
				try {
					this.mInputStream.close();
				} catch (IOException var5) {
				}
			}

			if (this.mOutputStream != null) {
				try {
					this.mOutputStream.close();
				} catch (IOException var4) {
				}
			}

			if (this.socket != null) {
				try {
					this.socket.close();
				} catch (IOException var3) {
				}
			}

			this.mInputStream = null;
			this.mOutputStream = null;
			this.socket = null;
			return 0;
		}
	}

	public int deviceConnect() throws IOException {

		synchronized (CommManagement.class) {
			int st;
			if (this.mIsConnected) {
				st = this.ReadVersion();
				switch (st) {
				case -19:
					st = this.ReadVersion();
					if (st == 0) {
						return 0;
					}
					break;
				case 0:
					return 0;
				}
			}

			this.mThreadPause = true;
			st = this.socketConn();
			if (st == 0) {
				this.delay(1000L);
				st = this.ReadVersion();
			}

			this.mThreadPause = false;
			this.mIsConnected = st == 0;
			return st;
		}
	}

	public int deviceConnect(BluetoothDevice device) {

		synchronized (CommManagement.class) {
			if (device != null) {
				if (this.mLastDevice != null
						&& this.mLastDevice.getAddress().equals(
								device.getAddress()) && this.mIsConnected) {
					int stReadVer = this.ReadVersion();
					switch (stReadVer) {
					case -19:
						stReadVer = this.ReadVersion();
						if (stReadVer == 0) {
							return 0;
						}
						break;
					case 0:
						return 0;
					}
				}

				if (this.mIsConnected) {
					this.bluetoothClose();
				}

				this.mThreadPause = true;
				NconnectType = 1;
				this.bluetoothConn(device);
				if (NconnectType == 0) {
					this.mLastDevice = device;
					this.delay(1000L);
					// NconnectType = this.ReadVersion();
				}

				this.mThreadPause = false;
				this.mIsConnected = NconnectType == 0;
				return NconnectType;
			} else {
				return -74;
			}
		}
	}

	private int socketConn() {

		synchronized (CommManagement.class) {
			LogMg.i("CommManagement", this + "start socketConn");
			NconnectType = 1;
			Set<?> pairedDevices = this._bluetooth.getBondedDevices();
			if (this.mLastDevice != null
					&& pairedDevices.contains(this.mLastDevice)) {
				LogMg.i("CommManagement", this
						+ " socketConn, connect to mLastDevice");
				this.bluetoothConn(this.mLastDevice);
				if (NconnectType < 0) {
					this.mLastDevice = null;
				}

				return NconnectType;
			} else {
				if (pairedDevices.size() > 0) {
					Iterator<?> var4 = pairedDevices.iterator();

					while (var4.hasNext()) {
						BluetoothDevice device = (BluetoothDevice) var4.next();
						if (device.getName() != null
								&& (device.getName().startsWith("Dual-SPP") || device
										.getName().startsWith("joesmate"))) {
							LogMg.i("CommManagement",
									this + " socketConn, device="
											+ device.getAddress());
							this.bluetoothConn(device);
							if (NconnectType == 0) {
								this.mLastDevice = device;
								return NconnectType;
							}
						}
					}
				}

				return NconnectType;
			}
		}
	}

	private void bluetoothConn(BluetoothDevice device) {

		synchronized (CommManagement.class) {
			LogMg.i("CommManagement", this
					+ "start bluetoothConn, NconnectType=" + NconnectType);

			try {

				NconnectType = this.open_device(device);
			} catch (IOException var4) {
				var4.printStackTrace();
			}

			if (NconnectType == 0) {
				LogMg.v("CommManagement", "openDevice OK");
			} else {
				LogMg.v("CommManagement", "openDevice Fail");
			}

			LogMg.i("CommManagement", this
					+ " bluetoothConn, return NconnectType=" + NconnectType);
		}
	}

	private int open_device(BluetoothDevice device) throws IOException {

		label97: {
			try {
				if (device != null) {
					this.socket = device
							.createRfcommSocketToServiceRecord(UUID
									.fromString("00001101-0000-1000-8000-00805F9B34FB"));
					if (socket.isConnected())
						socket.close();
					this.socket.connect();

				}
				break label97;
			} catch (IOException var11) {
				var11.printStackTrace();
				this.socket = null;
			} finally {
				if (this.socket != null) {

					try {
						this.mInputStream = this.socket.getInputStream();
						this.mOutputStream = this.socket.getOutputStream();
					} catch (IOException var10) {
						return -2;
					}
				}

			}

			return -1;
		}

		return this.socket != null ? 0 : -3;
	}

	public boolean isConnected() {
		return this.getIsConnected();
	}

	public boolean getIsConnected() {
		return this.mIsConnected;
	}

	/**
	 * 发送指令
	 **/
	public abstract int SendRecv(byte[] var1, int var2, byte[] var3, int[] var4);

	protected void bluetoothBroken() {
		this.bluetoothClose();
	}

	public int closeDevice() {
		this.mThreadPause = true;
		return this.bluetoothClose();
	}

	public void setCommTimeouts(int mSecTotal) {
		this.ulTotalTimeOuts = mSecTotal;
	}

	public String getVersion() {
		return this.mVersion;
	}

	protected void InputFulsh() throws Exception {

		int ix = this.mInputStream.available();
		if (ix > 0) {
			byte[] b = new byte[ix];
			this.mInputStream.read(b);
			Log.e("InputFulsh", "清空缓存：ix=%d,ix=%s");
		}
	}
}