package com.joesmate.sdk.reader;

import android.util.Log;

import com.joesmate.sdk.util.CmdCode;
import com.joesmate.sdk.util.ToolFun;

/*    */public abstract class CmdManagement
/*    */{
	/* 9 */protected byte[] sendBuffer = null;
	/* 10 */protected byte[] recvBuffer = null;
	/* 11 */protected CommManagement commMan = null;
	/* 12 */protected int secTimeout = 20000;
	/*    */
	/* 14 */protected boolean isBigData = false;

	protected abstract CommManagement getCommInstance();

	public void setBigData(boolean isbigData) {
		this.isBigData = isbigData;
	}
	public int SendRecvID(int timeout) {

		if (this.commMan != null) {

			this.commMan.isBigData = this.isBigData;

			this.commMan.setCommTimeouts(this.secTimeout);

			byte[] readdata;
			if (isBigData)
				readdata = new byte[CmdCode.MAX_FINGER_SIZE];
			else
				readdata = new byte[CmdCode.MAX_SIZE_BUFFER];

			int[] readLen = new int[3];

			if (!this.commMan.getIsConnected())
				return -102;

			int st = this.commMan.SendRecv(this.sendBuffer,
					this.sendBuffer.length, readdata, readLen);
			Log.e("身份证发送指令",ToolFun.printHexString(sendBuffer));
			if (st == 0) {

				this.recvBuffer = new byte[readLen[0]];

				System.arraycopy(readdata, 0, this.recvBuffer, 0, readLen[0]);
				

			} else if (st == -17) {

				try {

					Thread.sleep(4000L);

				} catch (Exception localException) {
				}

				st = this.commMan.SendRecv(this.sendBuffer,
						this.sendBuffer.length, readdata, readLen);

				if (st == 0) {

					this.recvBuffer = new byte[readLen[0]];

					System.arraycopy(readdata, 0, this.recvBuffer, 0,
							readLen[0]);

				}

			}

			this.commMan.setCommTimeouts(20000);

			return st;

		}

		return -73;

	}
	public int SendRecv() {

		if (this.commMan != null) {

			this.commMan.isBigData = this.isBigData;

			this.commMan.setCommTimeouts(this.secTimeout);

			byte[] readdata;
			if (isBigData)
				readdata = new byte[CmdCode.MAX_FINGER_SIZE];
			else
				readdata = new byte[CmdCode.MAX_SIZE_BUFFER];

			int[] readLen = new int[1];

			if (!this.commMan.getIsConnected())
				return -102;

			int st = this.commMan.SendRecv(this.sendBuffer,
					this.sendBuffer.length, readdata, readLen);
			if (st == 0) {

				this.recvBuffer = new byte[readLen[0]];

				System.arraycopy(readdata, 0, this.recvBuffer, 0, readLen[0]);

			} else if (st == -17) {

				try {

					Thread.sleep(4000L);

				} catch (Exception localException) {
				}

				st = this.commMan.SendRecv(this.sendBuffer,
						this.sendBuffer.length, readdata, readLen);

				if (st == 0) {

					this.recvBuffer = new byte[readLen[0]];

					System.arraycopy(readdata, 0, this.recvBuffer, 0,
							readLen[0]);

				}

			}

			this.commMan.setCommTimeouts(20000);

			return st;

		}

		return -73;

	}

	public void setCommTimeouts(int mSecTotal) {
		this.secTimeout = mSecTotal;

	}

	public byte[] getRecvData()
	/*    */{
		return this.recvBuffer;
	}
}
