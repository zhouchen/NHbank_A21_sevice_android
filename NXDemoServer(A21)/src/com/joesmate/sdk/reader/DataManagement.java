package com.joesmate.sdk.reader;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public abstract class DataManagement {
	protected CmdManagement cmdMan = null;
	protected JSONObject data = new JSONObject();

	public int execCmd() {
		synchronized (DataManagement.class) {
			if (this.cmdMan != null) {

				int st = this.cmdMan.SendRecvID(20);
				Log.e("下发指令", st + "");
				if (st != 0) {

					return st;
				}
			}

			int pRes = -74;
			try {
				pRes = parseResult();

			} catch (JSONException e) {
				e.printStackTrace();
			}
			return pRes;
		}
	}
	public int execCmdID(int timeout) {
		synchronized (DataManagement.class) {
			if (this.cmdMan != null) {

				int st = this.cmdMan.SendRecvID(timeout);
				if (st != 0) {

					return st;
				}
			}

			int pRes = -74;
			try {
				pRes = parseResult();

			} catch (JSONException e) {
				e.printStackTrace();
			}
			return pRes;
		}
	}

	protected abstract int parseResult() throws JSONException;

	public JSONObject getResult() {
		return this.data;
	}
}
