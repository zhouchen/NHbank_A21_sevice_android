/*     */
package com.joesmate.sdk.reader;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Message;
import android.text.format.Time;
import android.util.Log;

import com.joesmate.sdk.listener.ClipReturnListener;
import com.joesmate.sdk.listener.ReturnListener;
import com.joesmate.sdk.mtreader.BlueToothClass;
import com.joesmate.sdk.util.CmdCode;
import com.joesmate.sdk.util.LogMg;
import com.joesmate.sdk.util.RetCode;
import com.joesmate.sdk.util.ToolFun;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@SuppressLint("HandlerLeak")
public class EMVTrade {
	private static final EMVTrade sInstance = new EMVTrade();

	private EMVTrade() {
	}

	public static EMVTrade getInstance() {

		synchronized (EMVTrade.class) {
			return sInstance;
		}
	}

	private byte toBCD(int src) {
		return (byte) (src / 10 * 16 + src % 10);
	}


	private int readMagData(JSONObject jsonobj, int timeout) {
		MagCardMan magMan = new MagCardMan(timeout * 1000);
		int st = magMan.execCmd();
		
		if (st == 0 && jsonobj != null ) {
			JSONObject json = magMan.getResult();
			try {
                 
				String e = json.getString("Track2").trim();
				String track1 = json.getString("Track1").trim();
				String track3 = json.getString("Track3").trim();
                Log.e("一磁道：",e+"==="+track1+"==="+track3);
                
				int index = e.indexOf("=");
				if (index <= 0 && e.length()<=0 && track1.length()<=0&&track3.length()<=0) {
//					jsonobj.put("cardNo", "");
//					jsonobj.put("userName", "");
//					jsonobj.put("validity", "");
//					jsonobj.put("ErrCode", "-1");
//					jsonobj.put("ErrMsg", "刷卡失败");
//					jsonobj.put("cardtype", "1");
//					jsonobj.put("track1Data", "");
//					jsonobj.put("track2Data", "");
//					jsonobj.put("track3Data", "");
					Log.e("数据为空","");
					return -1;
                   
				} else {
				    if(e.length()>0 && track1.length()<=0&&track3.length()<=0){
				    	
					int start = e.charAt(0) >= 48 && e.charAt(0) <= 57 ? 0 : 1;
					String cardNo = e.substring(start+2, index);
					jsonobj.put("cardNo", cardNo);
					
					jsonobj.put("userName", "");
					jsonobj.put("validity", "");
					jsonobj.put("ErrCode", "0");
					jsonobj.put("ErrMsg", "刷卡成功");
					jsonobj.put("cardtype", "1");
					jsonobj.put("track1Data", track1);
					jsonobj.put("track2Data", e);
					jsonobj.put("track3Data", track3);
					return 0;
				    }else if(track1.length()>0 &&e.length()<=0 ){
				    	String cardno;
				    	 int ii= track1.indexOf("^");
				    	 String st1=track1.substring(0,1);
				    	 if(st1.equals("B")){
				    		 jsonobj.put("cardNo", track1.substring(1, ii)); 
				    	 }else if(st1.equals("9")){
						  cardno=track1.substring(2,ii);
						  jsonobj.put("cardNo", cardno);
				    	 }
							jsonobj.put("userName", "");
							jsonobj.put("validity", "");
							jsonobj.put("track1Data", track1);
							jsonobj.put("track2Data", e);
							jsonobj.put("track3Data", track3);
							jsonobj.put("ErrCode", "0");
							jsonobj.put("ErrMsg", "刷卡成功");
							jsonobj.put("cardtype", "1");
						 return 0;
				    }

				}
			} catch (JSONException var9) {
				var9.printStackTrace();
			}
		} else {
			return -2;
		}

		return st;
	}

	// 读取磁条卡语音
	public void ConnectSpeak() {
		// e8 bf 9e e6 8e a5 e8 ae be e5 a4 87
		// e8 af bb e5 8d a1 e5 a4 b1 e8 b4 a5
		byte[] cmd = new byte[] { (byte) 0xc0, (byte) 0x0e, (byte) 0xe8,
				(byte) 0xaf, (byte) 0xbb, (byte) 0xe5, (byte) 0x8d,
				(byte) 0xa1, (byte) 0xe5, (byte) 0xa4, (byte) 0xb1,
				(byte) 0xe8, (byte) 0xb4, (byte) 0xa5, (byte) 0x00,
				(byte) 0x00, (byte) 0x00 };
		MT3YCmdMan cmdMan = new MT3YCmdMan(cmd);
		int st = cmdMan.SendRecv();
	}
	// 读卡成功 e8 af bb e5 8d a1 e6 88 90 e5 8a 9f
	public void ConnectSpeakSuceess() {
		
		byte[] cmd = new byte[] { (byte) 0xc0, (byte) 0x0e, (byte) 0xe8,
				(byte) 0xaf, (byte) 0xbb, (byte) 0xe5, (byte) 0x8d,
				(byte) 0xa1, (byte) 0xe6, (byte) 0x88, (byte) 0x90,
				(byte) 0xe5, (byte) 0x8a, (byte) 0x9f, (byte) 0x00,
				(byte) 0x00, (byte) 0x00 };
		MT3YCmdMan cmdMan = new MT3YCmdMan(cmd);
	    cmdMan.SendRecv();
	}
	// 读取磁条卡
	// 磁条卡的读取
	String mag;
	
	@SuppressWarnings("rawtypes")
	public JSONObject readMg(final int timeout) {
		JSONObject json = new JSONObject();
		@SuppressWarnings("unused")
		Class var3 = EMVTrade.class;
		
		int st = MagCardDev.getInstance().magReadStart(timeout);
		
		ToolFun.Dalpey(200);
		if (st != 0) {
			try {
				Log.e("磁条卡不为空","4444");
				json.put("ErrMsg", RetCode.GetErrMsg(st));
				json.put("ErrCode", String.valueOf(st));
			} catch (JSONException e) {
			}
			return json;
		}
		long time1 = System.currentTimeMillis();

		while (System.currentTimeMillis() - time1 < (long) (timeout * 1000)) {
			int e = EMVTrade.this.readMagData(json, timeout);
			if (e == 0) {
				
				return json;
			}else{
				try {
					json.put("ErrCode", e);
					json.put("ErrMsg", "磁条卡获取失败");
					return json;
				} catch (Exception var6) {
					var6.printStackTrace();
				}
			}
		}
		try {
			json.put("ErrCode", "-75");
			json.put("ErrMsg", RetCode.GetErrMsg(-75));
			return json;
		} catch (Exception var6) {
			var6.printStackTrace();
		}
		return json;
	}

	// 接触式IC卡
	public String[] GetICCInfo(int ICType, String AIDList, String TagList,
			short strTimeout) {
		String[] arrRet = null;
		String[] string=new String[2];
		byte[] icType = new byte[1];
		icType[0] = (byte) ICType;
		byte[] aidList = AIDList.getBytes();

		byte[] tagList = TagList.getBytes();

		byte[] timeout = { (byte) strTimeout };

		byte[] cmd = mcCreatData((byte) 0x00, icType, aidList, tagList, timeout);

		MT3YCmdMan cmdMan = new MT3YCmdMan(cmd);
		cmdMan.setCommTimeouts(strTimeout * 1000);
		int st = cmdMan.SendRecv();
		if (st == 0) {
			byte[] recvBuffer = cmdMan.getRecvData();
			
			arrRet = new String(recvBuffer).split("\\|");
			Log.e("接触式IC卡信息======",ToolFun.printHexString(recvBuffer));
			if (arrRet[0].equals("0")) {
				return arrRet;
			}else{
				string[0]="-1";
				string[1]="读取IC卡失败";
				return string;
			}
		}else{
			string[0]=String.valueOf(st);
			string[1]=RetCode.GetErrMsg(st);
			
			return string;
		}
		
	}
	// PSAM卡
		public String[] GetPSAM(int ICType, String AIDList, String TagList,
				short strTimeout) {
			String[] arrRet = null;
			String[] string=new String[2];
			byte[] icType = new byte[1];
			icType[0] = (byte) ICType;
			byte[] aidList = AIDList.getBytes();

			byte[] tagList = TagList.getBytes();

			byte[] timeout = { (byte) strTimeout };

			byte[] cmd = mcCreatData((byte) 0x06, icType, aidList, tagList, timeout);

			MT3YCmdMan cmdMan = new MT3YCmdMan(cmd);
			cmdMan.setCommTimeouts(strTimeout * 1000);
			int st = cmdMan.SendRecv();
			if (st == 0) {
				byte[] recvBuffer = cmdMan.getRecvData();
				arrRet = new String(recvBuffer).split("\\|");
				if (arrRet[0].equals("0")) {
					return arrRet;
				}else{
					string[0]="-1";
					string[1]="读取IC卡失败";
					return string;
				}
			}else{
				string[0]=String.valueOf(st);
				string[1]=RetCode.GetErrMsg(st);
				
				return string;
			}
			
		}
	String[] arrRet = null;

	public String[] GetARQC(int ICType, String TxData, String AIDList,
			short strTimeout) {
		String[] string=new String[2];
		byte[] icType = new byte[1];
		icType[0] = (byte) ICType;
		byte[] txtdata = TxData.getBytes();

		byte[] aidList = AIDList.getBytes();

		byte[] timeout = new byte[1];
		timeout[0] = (byte) strTimeout;
		byte[] cmd = mcCreatData((byte) 0x01, icType, txtdata, aidList, timeout);

		MT3YCmdMan cmdMan = new MT3YCmdMan(cmd);
		cmdMan.setCommTimeouts(strTimeout * 1000);
		int st = cmdMan.SendRecv();
		
		if (st == 0) {
			byte[] recvBuffer = cmdMan.getRecvData();
			arrRet = new String(recvBuffer).split("\\|");
			if (arrRet[0].equals("0")) {
				return arrRet;
			}else{
				string[0]="-1";
				string[1]="读取55域失败";
				return string;
			}
		}else{
			string[0]=String.valueOf(st);
			string[1]=RetCode.GetErrMsg(st);
			return string;
		}
	}

	public String[] ARPCExeScript(int ICType, String TxData, String ARPC,
			String CDol2) {
		String[] arrRet = null;

		byte[] icType = { (byte) ICType };

		byte[] tagList = TxData.getBytes();

		byte[] arpc = ARPC.getBytes();

		byte[] cdol = CDol2.getBytes();

		byte[] cmd = mcCreatData((byte) 0x02, icType, tagList, arpc, cdol);

		MT3YCmdMan cmdMan = new MT3YCmdMan(cmd);
		int st = cmdMan.SendRecv();
		if (st == 0) {
			byte[] recvBuffer = cmdMan.getRecvData();
			arrRet = new String(recvBuffer).split("\\|");
			if (arrRet[0].equals("0")) {
				return arrRet;
			}

		}
		return arrRet;
	}

	public String[] GetTrDetail(int ICType, int NOLog, short strTimeout) {
		String[] arrRet = null;

		byte[] icType = { (byte) ICType };

		byte[] nolog = { (byte) NOLog };

		byte[] timeout = { (byte) strTimeout };

		byte[] cmd = mcCreatData((byte) 0x03, icType, nolog, timeout);

		MT3YCmdMan cmdMan = new MT3YCmdMan(cmd);
		cmdMan.setCommTimeouts(strTimeout * 1000);
		int st = cmdMan.SendRecv();
		if (st == 0) {
			byte[] recvBuffer = cmdMan.getRecvData();
			arrRet = new String(recvBuffer).split("\\|");
			if (arrRet[0].equals("0")) {
				return arrRet;
			}

		}
		return arrRet;
	}

	public String[] GetLoadLog(int ICType, int NOLog, String AIDList,
			short strTimeout) {
		String[] arrRet = null;

		byte[] icType = { (byte) ICType };

		byte[] nolog = { (byte) NOLog };

		byte[] aidList = AIDList.getBytes();

		byte[] timeout = { (byte) strTimeout };

		byte[] cmd = mcCreatData((byte) 0x04, icType, nolog, aidList, timeout);
		MT3YCmdMan cmdMan = new MT3YCmdMan(cmd);
		cmdMan.setCommTimeouts(strTimeout * 1000);
		int st = cmdMan.SendRecv();
		if (st == 0) {
			byte[] recvBuffer = cmdMan.getRecvData();
			arrRet = new String(recvBuffer).split("\\|");
			if (arrRet[0].equals("0")) {
				return arrRet;
			}

		}
		return arrRet;
	}

	public String[] SCCBA_GetICAndARQCInfo(int ICType, String AIDList,
			String TagList, String TxData, short strTimeout) {
		String[] arrRet = null;
		byte[] icType = { (byte) ICType };

		byte[] aidList = AIDList.getBytes();

		byte[] tagList = TagList.getBytes();

		byte[] txtdata = TxData.getBytes();

		byte[] timeout = { (byte) strTimeout };

		byte[] cmd = mcCreatData((byte) 0x00, icType, aidList, tagList,
				txtdata, timeout);

		MT3YCmdMan cmdMan = new MT3YCmdMan(cmd);
		cmdMan.setCommTimeouts(strTimeout * 1000);
		int st = cmdMan.SendRecv();
		if (st == 0) {
			byte[] recvBuffer = cmdMan.getRecvData();
			arrRet = new String(recvBuffer).split("\\|");
			if (arrRet[0].equals("0")) {
				return arrRet;
			}

		}
		return arrRet;
	}

	private byte[] mcCreatData(byte port, byte[]... params) {
		int len = 0;
		byte[] tmp = new byte[2048];
		for (byte[] item : params) {
			tmp[len] = (byte) item.length;
			len++;
			System.arraycopy(item, 0, tmp, len, item.length);
			len += item.length;
		}
		byte[] cmd = new byte[len + 3];
		int pos = 0;
		cmd[pos] = (byte) 0x50;
		pos++;
		cmd[pos] = (byte) 0x04;
		pos++;
		cmd[pos] = port;
		pos++;
		System.arraycopy(tmp, 0, cmd, pos, len);
		return cmd;
	}


	
}
