/*    */package com.joesmate.sdk.reader;

import android.util.Log;

import com.joesmate.sdk.listener.ClipReturnListener;
import com.joesmate.sdk.listener.ReturnListener;
import com.joesmate.sdk.util.ToolFun;

/*    */
public abstract class FingerDev {
	public abstract void regFingerPrint(
			final ReturnListener paramReturnListener, final int paramInt);

	public abstract String getFactory();

	protected byte[] recvBuffer = null;
	protected boolean isBigData = false;
	protected ClipReturnListener m_ClipReturnListener;
	protected ReturnListener m_ReturnListener;

	public abstract void sampFingerPrint(
			final ReturnListener paramReturnListener, final int paramInt);

	public abstract void imgFingerPrint(
			final ReturnListener paramReturnListener, final int paramInt);

	protected int fingerChannel(byte[] data, byte mode) {
		byte[] sendData = new byte[data.length + 3];
		sendData[0] = -58;
		sendData[1] = 2;
		sendData[2] = mode;
		System.arraycopy(data, 0, sendData, 3, data.length);
		Log.e("指纹数据下发=",ToolFun.printHexString(sendData));
		CmdManagement cmdMan = new MT3YCmdMan(sendData);
		cmdMan.secTimeout = 30000;
		int st = cmdMan.SendRecv();

		if (st == 0) {

			this.recvBuffer = cmdMan.getRecvData();

		}
		// Log.e("指纹下发获取特征码的指令",st+"");
		return st;
	}

	public int fingerSetBaudRate(int indxBaudRate) {
		byte[] sendData = { -58, 1, (byte) indxBaudRate };
		CmdManagement cmdMan = new MT3YCmdMan(sendData);
		int st = cmdMan.SendRecv();
		return st;
	}

	public void setBigData(boolean isbigData) {
		this.isBigData = isbigData;
	}
}
