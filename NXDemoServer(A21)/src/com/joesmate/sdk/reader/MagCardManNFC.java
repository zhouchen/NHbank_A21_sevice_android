package com.joesmate.sdk.reader;

import org.json.JSONException;

import com.joesmate.sdk.util.RetCode;

public class MagCardManNFC  extends DataManagement{
	public MagCardManNFC(int timeout) {
		byte[] cmd = { 50, 99, (byte) timeout };

		this.cmdMan = new MT3YCmdMan(cmd);
		cmdMan.secTimeout = timeout * 1000;
	}
	public MagCardManNFC(byte[] cmd) {
		this.cmdMan = new MT3YCmdMan(cmd);
	}

	public MagCardManNFC(CmdManagement cmd) {
		this.cmdMan = cmd;
	}

	protected int parseResult()// 磁条卡解析
			throws JSONException {
		if (this.cmdMan != null) {
			byte[] recvBuffer = this.cmdMan.getRecvData();
			this.data.put("Track1", "");
			this.data.put("Track2", "");
			this.data.put("Track3", "");
			if ((recvBuffer != null) && (recvBuffer.length > 2)) {
				if ((((recvBuffer[0] & 0xff) != 250) && ((recvBuffer[0] & 0xff) > 79))
						|| (((recvBuffer[1] & 0xff) != 250) && ((recvBuffer[1] & 0xff) > 40))
						|| (((recvBuffer[2] & 0xff) != 250) && ((recvBuffer[2] & 0xff) > 170))) {
					this.data.put("ErrCode", -60);
					this.data.put("ErrMsg", RetCode.GetErrMsg(-60));
					return -60;
				}
				int track1Len = 0;
				int track2Len = 0;
				int track3Len = 0;
				if ((recvBuffer[0] & 0xff) == 250)
					track1Len = recvBuffer[0] & 0xff;
				if ((recvBuffer[1] & 0xff) == 250)
					track2Len = recvBuffer[1] & 0xff;
				if ((recvBuffer[2] & 0xff) == 250)
					track3Len = recvBuffer[2] & 0xff;
				if ((track1Len > 0) && (track1Len <= 250)) {
					byte[] track1Data = new byte[track1Len];
					System.arraycopy(recvBuffer, 3, track1Data, 0, track1Len);
					String track1=new String(track1Data).trim();
					if(track1==null){
						this.data.put("Track1", "");
					}else if(!track1.contains("~")){
					this.data.put("Track1", new String(track1Data).trim());
					}
				}
				if ((track2Len > 0) && (track2Len <= 250)) {
					byte[] track2Data = new byte[track2Len];
					System.arraycopy(recvBuffer, 3 + track1Len, track2Data, 0,
							track2Len);
					String track2=new String(track2Data).trim();
					if(track2==null){
						this.data.put("Track2","" );
					}else if(!track2.contains("~")){
					this.data.put("Track2", track2);
					}
				}
				if ((track3Len > 0) && (track3Len <= 250)) {
					byte[] track3Data = new byte[track3Len];
					System.arraycopy(recvBuffer, 3 + track1Len + track2Len,
							track3Data, 0, track3Len);
					String track3=new String(track3Data).trim();
					if(track3==null){
						this.data.put("Track2","" );
					}else if(!track3.contains("~")){
					this.data.put("Track2", track3);
					}
					
				}

				return 0;
			}
		}
		this.data.put("ErrCode", -73);
		this.data.put("ErrMsg", RetCode.GetErrMsg(-73));
		return -73;
	}
}
