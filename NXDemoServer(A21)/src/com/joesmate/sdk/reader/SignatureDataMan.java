package com.joesmate.sdk.reader;

import org.json.JSONException;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import org.apache.*;

import sun.misc.BASE64Encoder;

import com.joesmate.sdk.util.RetCode;
import com.joesmate.sdk.util.ToolFun;

/**
 * Created by andre on 2017/7/27 .
 */

public class SignatureDataMan extends DataManagement {

	public SignatureDataMan(byte[] cmd, int timeout) {

		cmdMan = new MT3YCmdMan(cmd);
		cmdMan.setBigData(true);
		cmdMan.setCommTimeouts(timeout);
	}

	// 签字
	@Override
	protected int parseResult() throws JSONException {
		byte[] recvBuffer = this.cmdMan.getRecvData();
		Log.e("图片数据", ToolFun.printHexString(recvBuffer)+"");
		if (recvBuffer != null) {
			Log.e("图片数据", recvBuffer.length + "字符串=");
			// Bitmap bm=BitmapFactory.decodeByteArray(recvBuffer, 0,
			// recvBuffer.length);

			BASE64Encoder enc = new BASE64Encoder();

			String mes = enc.encodeBuffer(recvBuffer); // 使用BASE64编码
			this.data.put("SignaBMP", mes);

			Log.e("图片数据22", "字符串=");
			return RetCode.OP_OK;
		} else {
			return RetCode.ERR_USER_CANCEL;
		}

	}

}
