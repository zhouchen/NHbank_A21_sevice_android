package com.joesmate.sdk.reader;


import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;

import com.joesmate.sdk.util.LogMg;
import com.joesmate.sdk.util.RetCode;

/**
 * Created by andre on 2017/7/27 .
 */

public class SignatureDev {
	private SignatureDev() {
	}

	private static final SignatureDev sInstance = new SignatureDev();

	public static SignatureDev getInstance() {
		return sInstance;
	}

	private Bitmap mSignaImg = null;
	private String mSingStr = null;

//	final byte cmd[] = new byte[] { (byte) 0xc7, (byte) 0x04, (byte) 0x03,
//			(byte) 0x36, (byte) 0x30, (byte) 0x30, (byte) 0x03, (byte) 0x36,
//			(byte) 0x30, (byte) 0x30 };
//	private byte[] byt = null;

	// 获取签名屏数据
	public Bitmap getmSignaImg() {

		return mSignaImg;
	}

	// 获取签名屏数据
	public String getmSignaStr() {

		return mSingStr;
	}

	// 打开签名屏
	public int Start(int timeout) {
		final byte cmd[] = new byte[] { (byte) 0xc7, (byte) 0x01 };
		SignatureDataMan sd = new SignatureDataMan(cmd, timeout * 1000);
		int iRet = sd.execCmd();
		LogMg.e("打开签名屏", "" + iRet);
		if (iRet == 0) {
			
		}
		return iRet;
	}

	// 清空签名数据
	public int Clear(int timeout) {
		final byte cmd[] = new byte[] { (byte) 0xc7, (byte) 0x03 };
		SignatureDataMan sd = new SignatureDataMan(cmd, timeout * 1000);
		int iRet = sd.execCmd();
		return iRet;
	}

	// 保存并上传数据
	public int getSignature(int timeout) {
		final byte cmd[] = new byte[] { (byte) 0xc7, (byte) 0x06 };
		SignatureDataMan sd = new SignatureDataMan(cmd, timeout * 1000);
		int iRet = sd.execCmd();
		if (iRet == 0) {
			JSONObject json = sd.getResult();
			try {
				mSingStr = (String) json.get("SignaBMP");
			} catch (JSONException e) {
				return RetCode.ERR_DATAFORMAT;
			}
		}
		return iRet;
	}

	// 关闭签名屏

	public int Close(int timeout) {
		final byte cmd[] = new byte[] { (byte) 0xc7, (byte) 0x02 };
		SignatureDataMan sd = new SignatureDataMan(cmd, timeout * 1000);
		int iRet = sd.execCmd();
		return iRet;
	}

}
