package com.joesmate.sdk.util;



public class ReCode {

	public static String ErrMsg(int errCode) {
		switch (errCode) {
		case 1:
			return "平板未开启蓝牙";
		case 2:
			return "通讯失败";
		case 3:
			return "连接超时";
		case -0001:
			return "设备连接断开";
		case -0002:
			return "取消操作";
		case -0003:
			return "操作超时";
		case -1001:
			return "读取磁条卡/存折失败";
		case -1002:
			return "IC接触读取失败";
		case -1003:
			return "IC非接读取失败";
		case -1004:
			return "未放置银行卡";
		case -1101:
			return "读取身份证失败";
		case -1102:
			return "未放置身份证";
		case -1103:
			return "身份证图片解析错误";
		case -1105:
			return "证件类型错误，请放置身份证";
		case -1106:
			return "证件类型错误，请放置外国人居住证";
		case -1202:
			return "密码输入不一致";
		case -1203:
			return "输入密码不足6位";
		case -1204:
			return "工作密钥加密异常";
		case -1205:
			return "获取按键输入错误";
		case -1301:
			return "放置时间过短";
		case -1302:
			return "指纹读取失败";
		case -1303:
			return "指纹获取失败";
		case -1701:
			return "IC卡上电失败";
		case -1702:
			return "IC卡下电失败";
		case -1703:
			return "IC卡未上电";
		case -1704:
			return "数据通讯错误";
		case -1705:
			return "APDU交互错误";
		case -1706:
			return "IC卡外部认证错误";
		case -1707:
			return "55域标签解析错误";
		case -1801:
			return "脚本执行失败";
		case -1802:
			return "执行脚本错误";
		case -1901:
			return "公钥获取失败";
		case -2001:
			return "主密钥下载异常";
		case -2101:
			return "工作密钥下载异常";
		case -2201:
			return "打开签名屏失败";
		case -2202:
			return "获取电子签名数据失败";
		case -2203:
			return "清楚数据失败";
		case -2204:
			return "关闭签名屏失败";

		}
		return "";
	}

}
