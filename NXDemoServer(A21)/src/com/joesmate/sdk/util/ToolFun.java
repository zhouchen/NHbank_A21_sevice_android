/*    */
package com.joesmate.sdk.util;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

@SuppressLint("DefaultLocale")
public class ToolFun {
	final static String TAG = ToolFun.class.toString();
	 /**
	  * 字节数组转成16进制表示格式的字符串
	  * 
	  * @param byteArray
	  *            需要转换的字节数组
	  * @return 16进制表示格式的字符串
	  **/
	 public static String toHexString(byte[] byteArray) {
	  if (byteArray == null || byteArray.length < 1)
	   throw new IllegalArgumentException("this byteArray must not be null or empty");
	 
	  final StringBuilder hexString = new StringBuilder();
	  for (int i = 0; i < byteArray.length; i++) {
	   if ((byteArray[i] & 0xff) < 0x10)//0~F前面不零
	    hexString.append("0");
	   hexString.append(Integer.toHexString(0xFF & byteArray[i]));
	  }
	  return hexString.toString().toLowerCase();
	 }
	//16进制转为字符串
	/**

	 * 16进制直接转换成为字符串(无需Unicode解码)

	 * @param hexStr

	 * @return

	 */

	public static String hexStr2Str(String hexStr) {

	    String str = "0123456789ABCDEF";

	    char[] hexs = hexStr.toCharArray();

	    byte[] bytes = new byte[hexStr.length() / 2];

	    int n;

	    for (int i = 0; i < bytes.length; i++) {

	        n = str.indexOf(hexs[2 * i]) * 16;

	        n += str.indexOf(hexs[2 * i + 1]);

	        bytes[i] = (byte) (n & 0xff);

	    }

	    return new String(bytes);

	}
	public static int asc_hex(byte[] asc, byte[] hex, int asclen) {
		String ss = new String(asc);
		int string_len = ss.length();
		int len = asclen;
		if (string_len % 2 == 1) {
			ss = "0" + ss;
			len++;
		}
		for (int i = 0; i < len; i++) {
			hex[i] = ((byte) Integer.parseInt(ss.substring(2 * i, 2 * i + 2),
					16));
		}
		return 0;
	}

	/**
	 * 字符串转hex字符串
	 * 
	 * @throws UnsupportedEncodingException
	 */
	public static String strToHex(String str)
			throws UnsupportedEncodingException {
		return String.format("%x", new BigInteger(1, str.getBytes("UTF-8")));

	}

	public static String hex_split(byte[] hex, int blen) {

		String strsplit = "";
		byte[] asc = new byte[blen * 2];
		hex_asc(hex, asc, blen);
		String strasc = new String(asc);
		char[] charasc = strasc.toCharArray();
		for (char c : charasc) {
//			strsplit += "3" + c;
			strsplit +=c;
		}
		String str = new String(hexStringToBytes(strsplit));
		return str;
	}
	public static String hex_splitTC(byte[] hex, int blen) {

		String strsplit = "";
		byte[] asc = new byte[blen * 2];
		hex_asc(hex, asc, blen);
		String strasc = new String(asc);
		char[] charasc = strasc.toCharArray();
		for (char c : charasc) {
			strsplit += "3" + c;
		}
		String str = new String(hexStringToBytes(strsplit));
		return str;
	}
	public static byte[] hexStringToBytes(String hexString) {
		if (hexString == null || hexString.equals("")) {
			return null;
		}
		hexString = hexString.toUpperCase();
		int length = hexString.length() / 2;
		char[] hexChars = hexString.toCharArray();
		byte[] d = new byte[length];
		for (int i = 0; i < length; i++) {
			int pos = i * 2;
			d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));

		}
		return d;
	}

	private static byte charToByte(char c) {
		return (byte) "0123456789ABCDEF".indexOf(c);
	}
	   /**
     * byte数据转
     */
    public static int byteArrayToInt(byte[] b) {
        return b[1] & 0xFF |
                (b[0] & 0xFF) << 8;
    }

    public static byte[] intToByteArray(int a) {
        return new byte[]{
                (byte) ((a >> 8) & 0xFF),
                (byte) (a & 0xFF)
        };
    }
	public static String printHexString(byte[] b) {
		String a = "";
		for (int i = 0; i < b.length; i++) {
			String hex = Integer.toHexString(b[i] & 0xFF);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}

			a = a + hex;
		}

		return a;
	}

	public static Bitmap stringtoBitmap(String string) {
		// 将字符串转换成Bitmap类型
		Bitmap bitmap = null;
		try {
			byte[] bitmapArray;
			bitmapArray = Base64.decode(string, Base64.DEFAULT);
			bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0,
					bitmapArray.length);
			return bitmap;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int hex_asc(byte[] hex, byte[] asc, int blen) {
		for (int i = 0; i < blen; i++) {
			byte temp = (byte) (hex[i] & 0xF0);
			if (temp < 0) {
				temp = (byte) (hex[i] & 0x70);
				temp = (byte) ((byte) (temp >> 4) + 8);
			} else {
				temp = (byte) (hex[i] >> 4);
			}
			if ((temp >= 0) && (temp <= 9)) {
				asc[(i * 2 + 0)] = ((byte) (temp + 48));
			} else if ((temp >= 10) && (temp <= 15)) {
				asc[(i * 2 + 0)] = ((byte) (temp + 65 - 10));
			} else {
				asc[(i * 2 + 0)] = 48;
			}
			temp = (byte) (hex[i] & 0xF);
			if ((temp >= 0) && (temp <= 9)) {
				asc[(i * 2 + 1)] = ((byte) (temp + 48));
			} else if ((temp >= 10) && (temp <= 15)) {
				asc[(i * 2 + 1)] = ((byte) (temp + 65 - 10));
			} else
				asc[(i * 2 + 1)] = 48;
		}
		return 0;
	}

	public static int hex_asc(byte[] hex, int hexPos, byte[] asc, int blen) {
		for (int i = hexPos; i < hexPos + blen; i++) {
			byte temp = (byte) (hex[i] & 0xF0);
			if (temp < 0) {
				temp = (byte) (hex[i] & 0x70);
				temp = (byte) ((byte) (temp >> 4) + 8);
			} else {
				temp = (byte) (hex[i] >> 4);
			}
			if ((temp >= 0) && (temp <= 9)) {
				asc[((i - hexPos) * 2 + 0)] = ((byte) (temp + 48));
			} else if ((temp >= 10) && (temp <= 15)) {
				asc[((i - hexPos) * 2 + 0)] = ((byte) (temp + 65 - 10));
			} else {
				asc[((i - hexPos) * 2 + 0)] = 48;
			}
			temp = (byte) (hex[i] & 0xF);
			if ((temp >= 0) && (temp <= 9)) {
				asc[((i - hexPos) * 2 + 1)] = ((byte) (temp + 48));
			} else if ((temp >= 10) && (temp <= 15)) {
				asc[((i - hexPos) * 2 + 1)] = ((byte) (temp + 65 - 10));
			} else
				asc[((i - hexPos) * 2 + 1)] = 48;
		}
		return 0;
	}

	public static byte cr_bcc(int len, byte[] data) {
		byte temp = 0;

		for (int i = 0; i < len; i++)
			temp ^= data[i];

		return temp;

	}

	public static void Dalpey(long time) {
		try {
			Thread.sleep(time);
		} catch (Exception ex) {
			Log.e(TAG, "Daley: ", ex);
		}
	}

	public static void splitFun(byte[] usplitdata, byte ulen, byte[] splitdata,
			byte slen) {
		int nI = 0;
		for (int nJ = 0; nI < ulen * 2; nJ++) {
			splitdata[nI] = ((byte) (((usplitdata[nJ] & 0xF0) >> 4) + 48));
			splitdata[(nI + 1)] = ((byte) ((usplitdata[nJ] & 0xF) + 48));
			nI += 2;
		}
	}

	public static void saveBitmap(String path, Bitmap bm) {
		File f = new File(path);
		if (f.exists()) {
			f.delete();
		}
		try {
			FileOutputStream out = new FileOutputStream(f);
			bm.compress(Bitmap.CompressFormat.PNG, 90, out);
			out.flush();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static byte[] PackArg(byte[] head, byte[]... params) {
		int head_len = head.length;
		int len = 0;
		byte[] tmp = new byte[2048];
		for (byte[] item : params) {
			tmp[len] = (byte) item.length;
			++len;
			System.arraycopy(item, 0, tmp, len, item.length);
			len += item.length;
		}
		byte[] cmd = new byte[len + head_len];
		int pos = 0;
		System.arraycopy(head, 0, cmd, pos, head_len);
		pos += head_len;
		System.arraycopy(tmp, 0, cmd, pos, len);
		return cmd;
	}

	// bitmap转String
	public static String bitToStr(Bitmap bitmap) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();// outputstream
		bitmap.compress(CompressFormat.WEBP, 90, baos);
		byte[] appicon = baos.toByteArray();// 转为byte数组
		return Base64.encodeToString(appicon, Base64.DEFAULT);
	}

	// string 转bitmap
	public static Bitmap stringToBitmap(String string) {
		// 将字符串转换成Bitmap类型
		Bitmap bitmap = null;
		try {
			byte[] bitmapArray;
			bitmapArray = Base64.decode(string, Base64.DEFAULT);
			bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0,
					bitmapArray.length);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bitmap;
	}
	/**
	 * int转byte数组
	 * @param bytes
	 * @return
	 */
	public byte[]IntToByte(int num){
		byte[]bytes=new byte[4];
		bytes[0]=(byte) ((num>>24)&0xff);
		bytes[1]=(byte) ((num>>16)&0xff);
		bytes[2]=(byte) ((num>>8)&0xff);
		bytes[3]=(byte) (num&0xff);
		return bytes;
	}


}
